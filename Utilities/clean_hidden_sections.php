<?php

namespace SCGB;

use Exception;
use WP_Query;

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../SCGB/Utils/scgb_autoload.php';

try {
    Common::scgb_initialise('clean_hidden_sections');

    // Create a query of all public posts of type sc_resort
    $args = array(
        'posts_per_page'   => -1,
        'post_type' => 'sc_resort',
        'post_status' => 'publish',
        'orderby' => 'title',
        'order' => 'ASC',
    );

    // Iterate through the query and update the database with any data from the Ski Service API
    $query = new WP_Query($args);
    while ($query->have_posts())
    {
        $query->the_post();
        $changed = false;
        $post_id = get_the_ID();
        $post_title = html_entity_decode(get_the_title());
        Common::logger()->debug('Checking for hidden sections for ' . $post_title,
            array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,));
        $arr_meta_data = get_meta_data_for_post($post_id);
        $num_tabs = $arr_meta_data['resort_tabs'];

        // Number of tabs is not going to change - only changes will be
        for ($tab_number = 0; $tab_number < $num_tabs; $tab_number++) {
            $tab_name = $arr_meta_data['resort_tabs_' . $tab_number . '_tab_name'];
            $tab_section_data = unserialize($arr_meta_data[ 'resort_tabs_' . $tab_number . '_sections']);
            if (!is_array($tab_section_data)) {
                Common::logger()->warning('No tab section data for ' . $post_title . '/' . $tab_name,
                    array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,));
                continue;
            }
            $sections_to_keep = [];
            for ($tab_section_number = 0; $tab_section_number < count($tab_section_data); $tab_section_number++) {
                $meta_key = 'resort_tabs_' . $tab_number . '_sections_' . $tab_section_number . '_section_display_status';
                if (!array_key_exists($meta_key, $arr_meta_data)) {
                    Common::logger()->warning('No meta-data for ' . $post_title . '/' . $tab_name . '/' . $tab_section_number,
                        array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,));
                    $arr_meta_data[$meta_key] = 'all';
                }
                $section_status = $arr_meta_data[ 'resort_tabs_' . $tab_number . '_sections_' .
                    $tab_section_number . '_section_display_status'];
                if ($section_status != 'hide') {
                    $sections_to_keep[] = $tab_section_number;
                } else {
                    $changed = true;
                    Common::logger()->debug('Removing hidden section ' . $post_title . '/' . $tab_name . '/' . $tab_section_number,
                        array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,));
                }
            }

            // Now iterate through the meta-data and delete any for hidden sections
            // Need to move sections up to fill the gap
            if (count($sections_to_keep) != count($tab_section_data)) {
                // Need two passes - one to delete the hidden sections and one to move the remaining sections up
                foreach ($arr_meta_data as $meta_key => $meta_value) {
                    if (!str_starts_with($meta_key, 'resort_tabs_' . $tab_number . '_sections_'))
                        continue;

                    $key_fields = explode('_', $meta_key);
                    $current_section_number = $key_fields[4];
                    if (count(array_keys($sections_to_keep, $current_section_number)) == 0) {
                        unset($arr_meta_data[$meta_key]);
                        unset($arr_meta_data['_' . $meta_key]);
                    }
                }
                foreach ($arr_meta_data as $meta_key => $meta_value) {
                    if (!str_starts_with($meta_key, 'resort_tabs_' . $tab_number . '_sections_'))
                        continue;

                    $key_fields = explode('_', $meta_key);
                    $current_section_number = $key_fields[4];
                    $matches = array_keys($sections_to_keep, $current_section_number);
                    if (count($matches) == 0)
                        continue;
                    $new_section_number = $matches[0];
                    if ($new_section_number == $current_section_number)
                        continue;
                    // replace the section number in the meta_key
                    $new_meta_key = str_replace('_sections_' . $current_section_number . '_',
                        '_sections_' . $new_section_number . '_', $meta_key);
                    if (!key_exists('_' . $meta_key, $arr_meta_data) || !key_exists($meta_key, $arr_meta_data))
                    {
                        Common::logger()->error('Missing meta-data for ' . $post_title . '/' . $tab_name . '/' . $current_section_number,
                            array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,));
                        break;
                    }

                    $meta_value = $arr_meta_data[$meta_key];
                    $_meta_value = $arr_meta_data['_' . $meta_key];
                    $arr_meta_data[$new_meta_key] = $meta_value;
                    $arr_meta_data['_' . $new_meta_key] = $_meta_value;
                    unset($arr_meta_data[$meta_key]);
                    unset($arr_meta_data['_' . $meta_key]);
                }
            }
            $new_tab_section_data = [];
            for ($count = 0; $count < count($sections_to_keep); $count++) {
                $new_tab_section_data[] = $tab_section_data[$sections_to_keep[$count]];
            }
            $arr_meta_data['resort_tabs_' . $tab_number . '_sections'] = serialize($new_tab_section_data);
        }
        // Now update all the meta-data for this post
        if ($changed)
            update_resort_meta($post_id, $arr_meta_data);
    }

} catch (Exception $e) {
    Common::logger()->error($e->getMessage(),
        array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,));
}

try {
    Common::scgb_finalise(1);
} catch (Exception $e) {
    print_r('Failed while ending ' . $e->getMessage());
    die(1);
}
die(0);

/**
 * @throws Exception
 */
function update_resort_meta(int $post_id, array $arr_meta_data) : void
{
    global $wpdb;
    ksort($arr_meta_data);

    // First delete all the existing meta-data for this post
    $sql = "DELETE FROM wp_postmeta WHERE post_id = $post_id;";
    $result = $wpdb->query($sql);

    foreach ($arr_meta_data as $meta_key => $meta_value) {
        // escape the meta_value
        $meta_value = $wpdb->_escape($meta_value);
        $sql = "INSERT INTO wp_postmeta (post_id, meta_key, meta_value) VALUES ($post_id, '$meta_key', '$meta_value');";
        $result = $wpdb->query($sql);
    }
}

/**
 * @throws Exception
 */
function get_meta_data_for_post(int $post_id) : array
{
    global $wpdb;
    $sql = "SELECT meta_key, meta_value FROM wp_postmeta WHERE post_id = $post_id;";
    $result = $wpdb->get_results($sql);
    if ($result === false) {
        throw new Exception('Failed to get meta data for post: ' . $post_id);
    }
    $arr_meta_data = [];
    foreach ($result as $row) {
        $meta_key = $row->meta_key;
        $meta_value = $row->meta_value;
        $arr_meta_data[$meta_key] = $meta_value;
    }
    return $arr_meta_data;
}