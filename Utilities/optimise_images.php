<?php

namespace SCGB;

use Exception;

use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Spatie\ImageOptimizer\OptimizerChainFactory;

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../SCGB/Utils/scgb_autoload.php';

const MAX_IMAGE_SIZE = 200 * 1024; // 200Kb

try {
    Common::scgb_initialise('optimise images');

    // Iterate through files in the uploads directory and optimise them
    $upload_dir = wp_get_upload_dir();
    $upload_dir = $upload_dir['basedir'];

    $optimizerChain = OptimizerChainFactory::create();

    $directory = new RecursiveDirectoryIterator($upload_dir);
    $iterator = new RecursiveIteratorIterator($directory);
    foreach ($iterator as $info) {
        $path = $info->getPathname();
        if (!preg_match( '#wp-content/uploads/202[0-9]#', $path)) {
            continue;
        }
        // if a directory or not an image, skip it
        if ($info->isDir() || !in_array($info->getExtension(), array('jpg', 'jpeg', 'png', 'gif'))) {
            continue;
        }

        Common::logger()->debug('Checking image ' . $path);

        // See if it's a WordPress generated thumbnail - if so skip it - we will recreate these later
        if (preg_match('#-([0-9]+)x([0-9]+)\.#', $path)) {
            $orig_path = preg_replace('#-([0-9]+)x([0-9]+)\.#', '.', $path);
            if (file_exists($orig_path)) {
                Common::logger()->debug('Skipping thumbnail ' . $path);
                continue;
            }
        }

        // Get the image size - we want a max of 1280x1280
        $orig_size = $info->getSize();
        $image = imagecreatefromstring(file_get_contents($info->getPathname()));
        $width = imagesx($image);
        $height = imagesy($image);
        $tmp_name = "/tmp/" . basename($info->getPathname());
        if ($width > 1280 || $height > 1280) {
            if (resize_image($info->getPathname(), 1280, 1280, $tmp_name))
                Common::logger()->debug('Resizing image ' . $info->getPathname() . ' from ' . $width . 'x' . $height);
        }

        $optimizerChain->optimize($info->getPathname(), $tmp_name);
        $image_size = filesize($tmp_name);

        if ($image_size > MAX_IMAGE_SIZE) {
            $orig_size = $image_size;
            $image = imagecreatefromstring(file_get_contents($tmp_name));

            // Check whether still over MAX_IMAGE_SIZE - if so then degrade the quality
            $quality = 100;
            Common::logger()->debug('Degrading image ' . $info->getPathname() . ' from ' . $image_size);
            do {
                $temp_stream = fopen('php://temp', 'w+');
                imagejpeg($image, $temp_stream, $quality--);
                rewind($temp_stream);
                $fstat = fstat($temp_stream);
                fclose($temp_stream);

                $image_size = $fstat['size'];

            } while ($image_size > MAX_IMAGE_SIZE && $quality > 0);
                if ($quality > 0) {
                imagejpeg($image, $tmp_name, $quality++);
                Common::logger()->debug('Degraded image ' . $info->getPathname() .
                    ' from ' . $orig_size . ' to ' . $image_size);
            } else {
                Common::logger()->debug('Failed to degrade image ' . $info->getPathname() . ' to ' . $image_size);
            }
        }

        // If the image is smaller than the original, copy it back
        if ($image_size < $orig_size) {
            copy($tmp_name, $info->getPathname());
        }
        unlink($tmp_name);
        if ($image_size != $orig_size) {
            Common::logger()->debug('Optimised image ' . $info->getPathname() . ' from ' . $orig_size . ' to ' . $image_size);
        }
    }
} catch (Exception $e) {
    Common::logger()->error($e->getMessage(),
        array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,));
}

try {
    Common::scgb_finalise(1);
} catch (Exception $e) {
    print_r('Failed while ending ' . $e->getMessage());
    die(1);
}
die(0);

function resize_image($image_path,$new_width,$new_height, $new_image_path) : bool
{
    $mime = getimagesize($image_path);
    $result = false;

    $src_img = null;
    if($mime['mime']=='image/png') {
        $src_img = imagecreatefrompng($image_path);
    }
    if($mime['mime']=='image/jpg' || $mime['mime']=='image/jpeg' || $mime['mime']=='image/pjpeg') {
        $src_img = imagecreatefromjpeg($image_path);
    }

    $old_x          =   imageSX($src_img);
    $old_y          =   imageSY($src_img);
    $thumb_w        =   null;
    $thumb_h        =   null;

    if($old_x > $old_y)
    {
        $thumb_w    =   $new_width;
        $thumb_h    =   $old_y*($new_height/$old_x);
    }

    if($old_x < $old_y)
    {
        $thumb_w    =   $old_x*($new_width/$old_y);
        $thumb_h    =   $new_height;
    }

    if($old_x == $old_y)
    {
        $thumb_w    =   $new_width;
        $thumb_h    =   $new_height;
    }

    $dst_img        =   ImageCreateTrueColor($thumb_w,$thumb_h);

    imagecopyresampled($dst_img,$src_img,0,0,0,0,
        intval($thumb_w),intval($thumb_h),intval($old_x),intval($old_y));

    if($mime['mime']=='image/png') {
        $result = imagepng($dst_img,$new_image_path,8);
    }
    if($mime['mime']=='image/jpg' || $mime['mime']=='image/jpeg' || $mime['mime']=='image/pjpeg') {
        $result = imagejpeg($dst_img,$new_image_path,85);
    }

    imagedestroy($dst_img);
    imagedestroy($src_img);

    if (!$result) {
        unlink($new_image_path);
        return false;
    }


    $result = false;
    $old_size = filesize($image_path);
    $new_size = filesize($new_image_path);
    if ($new_size < $old_size) {
        $result = true;
        copy($new_image_path, $image_path);
    }
    unlink($new_image_path);
    return $result;
}


