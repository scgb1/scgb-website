<?php
declare(strict_types=1);
namespace SCGB;

use Exception;

/**
 * Fetch and process resort data from SkiResort Service.
 *
 * The SkiService API supplies XML containing data about the resorts they support.
 * The API is permissioned by source IP address (no other credentials)
 * The schema is detailed in the document 'SkiResort-XML-interface_documentation_engl.pdf'
 * which is in the docs directory
 *
 * They have their own resort id and resort names. Each row in the WP data file contains
 * that ID if data is available for that resort
 */
class SkiService
{
    /**
     * The Ski Service API URL
     */
    const RESORT_INFO_URL = 'https://www.skiresort-service.com/xml-feed/';

    private array $arrSkiServiceResortData = array();

    /**
     * Fetch XML data from Ski Service - either from their API or a file (if configured).
     *
     * @throws Exception
     */
    public function __construct()
    {
        /* See whether data is loaded from the API or from a file */
        /* Determined by the environment variable LOAD_SKI_SERVICE_DATA_FROM_FILE */
        $loadResortInfoFromFile = $_ENV['SKI_SERVICE_DATA_FROM_FILE'] ?? false;
        if ($loadResortInfoFromFile) {
            Common::logger()->debug(
                "Loading Ski Service data from file: " . $loadResortInfoFromFile,
                array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,)
            );
            $strSkiServiceResortDataXML = file_get_contents($loadResortInfoFromFile);
        } else {
            Common::logger()->debug(
                "Loading Ski Service data from API",
                array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,)
            );
            /* Download the Ski Service data from their API */
            $strSkiServiceResortDataXML = Common::curl(SkiService::RESORT_INFO_URL);

            /*
             * Check we got data back - if we didn't it's most likely a permission issue on the API.
             * This API is reliable so do not retry.
             */
            if (strlen($strSkiServiceResortDataXML) == 0) {
                Common::logger()->error(
                    "No data returned from Ski Service API - Check permissions",
                    array(
                        'url' => SkiService::RESORT_INFO_URL,
                        'ip' => getHostByName(getHostName()),
                        'file' => basename(__FILE__),
                        'function' => __FUNCTION__,
                        'line' => __LINE__,
                    )
                );
                throw new Exception("No data returned from Ski Service API - " .
                    "check permissions for this IP address");
            }

            // If set save the XML - determined by the environment variable SAVE_SKI_SERVICE_DATA_TO_FILE
            $strResortInfoXMLFile = $_ENV['SKI_SERVICE_DATA_TO_FILE'] ?? false;
            if ($strResortInfoXMLFile) {
                // Add timestamps to the filenames
                $strResortInfoXMLFile = Common::getTimeStampedFilename($strResortInfoXMLFile);
                Common::logger()->debug(
                    "Saving Ski Service data to file: ",
                    array(
                        'xml file' => $strResortInfoXMLFile,
                        'file' => basename(__FILE__),
                        'function' => __FUNCTION__,
                        'line' => __LINE__,
                    )
                );
                file_put_contents($strResortInfoXMLFile, $strSkiServiceResortDataXML);
            }
        }

        /* now turn it into an array */
        $xml = simplexml_load_string($strSkiServiceResortDataXML, "SimpleXMLElement", LIBXML_NOCDATA);
        $json = json_encode($xml);
        $arrSkiServiceRawData = json_decode($json, true);
        $arrSkiServiceRawData = $arrSkiServiceRawData['schneemeldung'];
        Common::logger()->debug(
            count($arrSkiServiceRawData) . " resorts in Ski Service data",
            array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,)
        );

        // If set save the data to timestamped XML and CSV files if loaded from the API
        $strResortInfoCSVFile = $_ENV['SKI_SERVICE_DATA_CSV_FILE'] ?? false;
        if ($strResortInfoCSVFile) {
            $strResortInfoCSVFile = Common::getTimeStampedFilename($strResortInfoCSVFile);
            // save the array $arrSkiServiceResortData as a CSV file into $strResortInfoCSVFile
            Common::logger()->debug(
                "Saving Ski Service data to file: " . $strResortInfoCSVFile,
                array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,)
            );
            $fp = fopen($strResortInfoCSVFile, 'w');
            fputcsv($fp, array_keys(reset($arrSkiServiceRawData)));

            foreach ($arrSkiServiceRawData as $fields) {
                $x = $fields;
                // Convert any arrays in $x to empty strings
                foreach ($x as $key => $value) {
                    if (is_array($value)) {
                        $x[$key] = '';
                    }
                }
                fputcsv($fp, $x);
            }
            fclose($fp);
            // Create a symlink to the latest file called 'SkiServiceData.csv'. If it already exists delete it first
            $strLatestFile = dirname($strResortInfoCSVFile) . '/SkiServiceData.csv';
            if (file_exists($strLatestFile)) {
                unlink($strLatestFile);
            }
            symlink(basename($strResortInfoCSVFile), $strLatestFile);
        }
        $this->extractResortData($arrSkiServiceRawData);
        return $this;
    }

    /**
     * Return the Ski Service resort data
     *
     * @returns array
     */
    public function getSkiServiceResortData(): array
    {
        return $this->arrSkiServiceResortData;
    }

    /**
     * Parse the XML, turn the German keys into English ones and perform any necessary translations.
     *
     * This creates another array that is keyed on the Ski Service resort ID and contains the fields of interest
     *
     * @param array $arrSkiServiceRawData - Array created direct from the XML
     * @returns void
     * @throws Exception
     */
    public function extractResortData(array $arrSkiServiceRawData): void
    {
        foreach ($arrSkiServiceRawData as $arrSkiServiceDataItem) {
            Common::logger()->debug(
                "Processing Ski Service resort",
                array(
                    'id' => $arrSkiServiceDataItem['region_id'],
                    'name' => html_entity_decode($arrSkiServiceDataItem['region_name']),
                    'file' => basename(__FILE__),
                    'function' => __FUNCTION__,
                    'line' => __LINE__,
                )
            );

            $intSkiServiceResortID = $arrSkiServiceDataItem['region_id'];
            $arrSkiServiceData = array(
                'intResortInfoID' => $intSkiServiceResortID,
                'strResortName' => $arrSkiServiceDataItem['region_name'],
                'intSnowLowerPiste' => $arrSkiServiceDataItem['schneehoehe_tal'],
                'intSnowUpperPiste' => $arrSkiServiceDataItem['schneehoehe_berg'],
                'dtmLastSnowed' => $arrSkiServiceDataItem['letzter_schneefall'],
                'intTotalLifts' => $arrSkiServiceDataItem['lifte_gesamt'],
                'intLiftsOpen' => $arrSkiServiceDataItem['offene_lifte'],
                'strOffPisteSnowConditions' => $this->englishSnowQuality($arrSkiServiceDataItem['schneequalitaet']),
                'dtmResortClosing' => $arrSkiServiceDataItem['datum_saisonende'],
                'dtmResortOpening' => $arrSkiServiceDataItem['datum_saisonstart'],
            );
            $this->arrSkiServiceResortData[$intSkiServiceResortID] = $arrSkiServiceData;
        }
    }

    /**
     * Convert the German snow quality to English.
     *
     * @param string $germanSnowQuality
     * @return string
     * @throws Exception
     */
    public function englishSnowQuality(string $germanSnowQuality): string
    {
        $germanToEnglishSnowMapping = array(
            'pulverschnee' => 'Powder snow',
            'neuschnee' => 'New snow',
            'altschnee' => 'Old snow',
            'kunstschnee' => 'Artificial snow',
            'sulz/harsch' => 'Heavy snow/snow crust',
            'firn' => 'Firm',
            'nassschnee' => 'Wet snow',
            'griffig' => 'Hard packed',
            'frühlingsschnee' => 'Spring snow',
            'keine angabe' => 'No information',
        );

        $lowerCaseGermanSnowQuality = strtolower($germanSnowQuality);
        // Check against lower case
        if ($germanSnowQuality && array_key_exists($lowerCaseGermanSnowQuality, $germanToEnglishSnowMapping)) {
            $englishSnowConditions = $germanToEnglishSnowMapping[$lowerCaseGermanSnowQuality];
        } else {
            Common::logger()->error(
                "Unknown snow quality: " . $germanSnowQuality,
                array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,)
            );
            $englishSnowConditions = 'No information';
        }
        return $englishSnowConditions;
    }

    /**
     * @throws Exception
     */
    public function updateResortData(int $post_id, int $resort_ski_service_id): void
    {
        // Get the WordPress database connection
        global $wpdb;

        // Get the resort data from the Ski Service data
        $arrSkiServiceResortData = $this->getSkiServiceResortData();
        $arrSkiServiceResortDataItem = $arrSkiServiceResortData[$resort_ski_service_id];

        // Do an update or insert into the table scgb_resort_data
        $intSnowLowerPiste = is_array($arrSkiServiceResortDataItem['intSnowLowerPiste'])
            ? "NULL" : $arrSkiServiceResortDataItem['intSnowLowerPiste'];
        $intSnowUpperPiste = is_array($arrSkiServiceResortDataItem['intSnowUpperPiste'])
            ? "NULL" : $arrSkiServiceResortDataItem['intSnowUpperPiste'];
        $dtmLastSnowed = is_array($arrSkiServiceResortDataItem['dtmLastSnowed'])
            ? "NULL" : "'" . $arrSkiServiceResortDataItem['dtmLastSnowed'] . "'";
        $offPisteSnowConditions = is_array($arrSkiServiceResortDataItem['strOffPisteSnowConditions'])
            ? "NULL" : "'" . $arrSkiServiceResortDataItem['strOffPisteSnowConditions'] . "'";
        $intLiftsOpen = is_array($arrSkiServiceResortDataItem['intLiftsOpen'])
            ? "NULL" : $arrSkiServiceResortDataItem['intLiftsOpen'];
        $intTotalLifts = is_array($arrSkiServiceResortDataItem['intTotalLifts'])
            ? "NULL" : $arrSkiServiceResortDataItem['intTotalLifts'];
        $dtmResortOpening = is_array($arrSkiServiceResortDataItem['dtmResortOpening'])
            ? "NULL" : "'" . $arrSkiServiceResortDataItem['dtmResortOpening'] . "'";
        $dtmResortClosing = is_array($arrSkiServiceResortDataItem['dtmResortClosing'])
            ? "NULL" : "'" . $arrSkiServiceResortDataItem['dtmResortClosing'] . "'";

        $sql = "INSERT INTO scgb_resort_data (intSCGBResortID, dtmLastSkiServiceUpdate, " .
            "intSnowLowerPisteInCentimeters, intSnowUpperPisteInCentimeters, dtmLastSnowed, " .
            "strOffPisteSnowConditions, intLiftsOpen, intLiftsTotal, dtmResortOpening, dtmResortClosing) " .
            "VALUES (" .
                $post_id . ", NOW(), " .
                $intSnowLowerPiste . ", " .
                $intSnowUpperPiste . ", " .
                $dtmLastSnowed . ", " .
                $offPisteSnowConditions . ", " .
                $intLiftsOpen . ", " .
                $intTotalLifts . ", " .
                $dtmResortOpening . ", " .
                $dtmResortClosing . ") " .
            "ON DUPLICATE KEY UPDATE 
                dtmLastSkiServiceUpdate = NOW(),
                intSnowLowerPisteInCentimeters = $intSnowLowerPiste,
                intSnowUpperPisteInCentimeters = $intSnowUpperPiste,
                dtmLastSnowed = $dtmLastSnowed,
                strOffPisteSnowConditions = $offPisteSnowConditions,
                intLiftsOpen = $intLiftsOpen,
                intLiftsTotal = $intTotalLifts,
                dtmResortOpening = $dtmResortOpening,
                dtmResortClosing = $dtmResortClosing
                ;";

        $x = $wpdb->query($sql);

        if (!$x) {
            Common::logger()->error(
                "Error updating resort data",
                array(
                    'post_id' => $post_id,
                    'resort_ski_service_id' => $resort_ski_service_id,
                    'file' => basename(__FILE__),
                    'function' => __FUNCTION__,
                    'line' => __LINE__,
                )
            );
        }
    }
}
