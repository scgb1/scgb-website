<?php
/** @noinspection SqlInsertValues */
declare(strict_types=1);
namespace SCGB;

use DateTime;
use Exception;
use InvalidArgumentException;

/**
 * Class to hold Resort Data. This is initially populated from a CSV and then enriched with elevation data.
 */
class WeatherData
{
    /**
     * Properties containing data loaded from the SkiClub file - The column headers must match these names.
     */
    protected int $intSCGBResortID = 0;          // Mandatory
    protected array $resort_forecast = array(); // Mandatory
    protected array $resort_higher_forecast = array(); // Mandatory

    /**
     * Current Weather - extracted from the Weather forecast and added while processing the forecasts
     */
    protected ?float $fltMinTemperature = null;
    protected ?float $fltMaxTemperature = null;
    protected string $strWeather = '';
    protected string $strWeatherURL = '';
    protected float $fltSnowForecastNext3DaysInCentimeters = 0.0;
    protected float $fltSnowForecastNext6DaysInCentimeters = 0.0;

    /**
     * Used to check for duplicates - not exposed outside the class
     */
    private static array $arrResortDataByID = array(); // array of intSCGBResortID

    /**
     * Constructor - takes an associated array containing a line from the Resort Data CSV.
     *
     * The config item 'resortDataFile' points at the CSV
     *
     * @param int $post_id
     * @param array|null $resort_forecast
     * @param array|null $resort_higher_forecast
     * @throws Exception
     */
    public function __construct(int $post_id, ?array $resort_forecast, ?array $resort_higher_forecast)
    {
        $this->intSCGBResortID = $post_id;

        // check $intSCGBResortID is positive and not a dupe
        if ($this->intSCGBResortID <= 0) {
            throw new Exception("Invalid SCGBResortID " . $this->intSCGBResortID);
        }
        if (array_key_exists($this->intSCGBResortID, self::$arrResortDataByID)) {
            throw new Exception("Duplicate SCGBResortID " . $this->intSCGBResortID);
        }
        self::$arrResortDataByID[$this->intSCGBResortID] = $this;

        if ($resort_forecast === null) {
            Common::logger()->error(
                "No resort lat long for " . $this->intSCGBResortID,
                array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,)
            );
            return $this;
        } else
        {
            $this->resort_forecast =  $this->process_forecast($resort_forecast, false);
        }

        if ($resort_higher_forecast === null) {
            Common::logger()->warning(
                "No higher lat long for resort " . $this->intSCGBResortID,
                array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,)
            );
        } else {
            $this->resort_higher_forecast = $this->process_forecast($resort_higher_forecast, true);
        }

            return $this;
    }

    public function is_valid(): bool
    {
        $status = true;
        if (count($this->resort_forecast) < 27 ||
            ($this->resort_higher_forecast && count($this->resort_higher_forecast) <27)) {
            $status = false;
        }
        return $status;
    }

    /**
     * Add weather forecast to the resort.
     *
     * We get an array of WeatherForecastBase objects and need to select the ones we want.
     *
     * @param array $arrForecasts
     * @param bool $isTopForecast
     * @return array
     * @throws Exception
     */
    private function process_forecast(array $arrForecasts, bool $isTopForecast): array
    {
        Common::logger()->debug(
            "Processing forecast for resort " . $this->intSCGBResortID .
            " isTopForecast " . $isTopForecast,
            array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,)
        );

        // iterate through the forecast data and pick out the times we want
        // Asked for 11 days (to allow for timezone shifts but only need 10
        foreach ($arrForecasts as $forecast) {
            $forecast->setIsTopForecast($isTopForecast);

            // Get the dates of the forecast - and then get current time in the same timezone
            $dtmForecast = $forecast->getForecastDate();
            $dtmForecastEnd = $forecast->getForecastEndDate();

            // Get the current time in the same timezone as the forecast
            $dtmNow = new DateTime();
            $timezone = $dtmForecast->getTimezone();
            $dtmNow->setTimezone($timezone);

            // Update the 3 & 6 day Snow Forecast on the resort - need to be timezone aware
            // Only do this for forecasts from the highest point on the resort
            // Keep track of the snow for the period not displayed
            $fltSnowExpected = $forecast->getSnowExpectedInCentimeters();
            if ($isTopForecast &&  $fltSnowExpected > 0.0 && $dtmForecastEnd > $dtmNow) {
                $this->addSnow($dtmNow, $dtmForecast, $fltSnowExpected);
            }

            // See if it's a period of interest - if it is get a string indicating the period
            $forecast->setForecastPeriod($dtmForecast, $dtmForecastEnd);

            // Update the resort with the current weather. The preference is to use the resort forecast
            // rather than the top forecast. But occasionally the resort forecast is not available
            if ($dtmNow >= $dtmForecast && $dtmNow < $dtmForecastEnd) {
                if (!$isTopForecast || $this->strWeather == '') {
                    // Update the current weather on the resort
                    $this->strWeather = $forecast->getWeather();
                    $this->fltMinTemperature = $forecast->getMinTemperature();
                    $this->fltMaxTemperature = $forecast->getMaxTemperature();
                    $this->strWeatherURL = $forecast->getWeatherURL();
                }
            }
        }

        // Finally prune the array to remove forecasts without a forecast period and the additional days
        // at beginning and end which we allowed for timezone shifts
        $day = 0;
        $final_array = array();
        foreach ($arrForecasts as $forecast) {
            if ($forecast->getForecastPeriod() == 'AM') {
                $day++;
            }
            if ($day > 0 && $day <= 10 && $forecast->getForecastPeriod() !== null) {
                $final_array[] = $forecast;
            }
        }
        return $final_array;
    }

    /**
     * Static function to write resort and forecast data to the database.
     *
     * @return void
     * @throws Exception
     */
    public function save_to_wordpress(): void
    {
        global $wpdb;
        Common::logger()->debug(
            "writeResortDataToDB: Writing resort data to database - resort " . $this->intSCGBResortID,
            array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,)
        );

        // Do an UPDATE OR INSERT on scgb_resort_data
        // We DO NOT update the last update time - this is done in the forecast table
        // dtmLastUpdate will be set by the Ski Service Job
        $sql = "INSERT INTO scgb_resort_data (intSCGBResortID, dtmLastUpdate, fltMinTemperature, fltMaxTemperature,
                              strWeather, strWeatherURL, fltSnowForecastNext3DaysInCentimeters,
                              fltSnowForecastNext6DaysInCentimeters) 
                    VALUES (%d, now(), %f, %f, %s, %s, %f, %f) 
                    ON DUPLICATE KEY UPDATE
                        dtmLastUpdate = now(),
                        fltMinTemperature = %s, 
                        fltMaxTemperature = %s, 
                        strWeather = %s, 
                        strWeatherURL = %s,
                        fltSnowForecastNext3DaysInCentimeters = %f,
                        fltSnowForecastNext6DaysInCentimeters = %f;";

        $sql = $wpdb->prepare(
            $sql,
            $this->intSCGBResortID,
            $this->fltMinTemperature,
            $this->fltMaxTemperature,
            $this->strWeather,
            $this->strWeatherURL,
            $this->fltSnowForecastNext3DaysInCentimeters,
            $this->fltSnowForecastNext6DaysInCentimeters,
            $this->fltMinTemperature,
            $this->fltMaxTemperature,
            $this->strWeather,
            $this->strWeatherURL,
            $this->fltSnowForecastNext3DaysInCentimeters,
            $this->fltSnowForecastNext6DaysInCentimeters
        );
        $wpdb->query($sql);

        // Write forecast data to the database - delete any existing data first
        // All in one transaction
        foreach (array(0,1) as $isTopForecast) {
            $array_forecast = $isTopForecast ? $this->resort_higher_forecast : $this->resort_forecast;

            if ($isTopForecast && $array_forecast == null) {
                Common::logger()->warning(
                    "writeResortDataToDB: No higher forecast data for resort " . $this->intSCGBResortID .
                    " isTopForecast " . $isTopForecast,
                    array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,));
                continue;
            }
            if ($array_forecast == null) {
                Common::logger()->error(
                    "writeResortDataToDB: No forecast data for resort " . $this->intSCGBResortID .
                    " isTopForecast " . $isTopForecast,
                    array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,));
                    continue;
            }
            $sql = "START TRANSACTION;";
            $wpdb->query($sql);
            $sql = "DELETE FROM scgb_resort_forecast WHERE intSCGBResortID = %d AND blnIsTopForecast = %d;\n";
            $sql = $wpdb->prepare($sql, $this->intSCGBResortID, $isTopForecast);
            $wpdb->query($sql);
            foreach ($array_forecast as $forecast) {
                $sql = "INSERT INTO scgb_resort_forecast (intSCGBResortID, dtmLastUpdate, dtmForecastDate, " .
                    "blnIsTopForecast, strForecastPeriod, strWeather, strWeatherURL, intWindSpeedInMPH, " .
                    "strWindDirection, strWindURL, fltSnowExpectedInCentimeters, fltMinTemperature, " .
                    "fltMaxTemperature, fltFeelsLikeTemperature) " .
                    "VALUES (%d, NOW(), %s, %d, %s, %s, %s, %d, %s, %s, %f, %f, %f, %f);\n";
                $sql = $wpdb->prepare($sql, $this->intSCGBResortID,
                    $forecast->getForecastDate()->format('Y-m-d'), $isTopForecast, $forecast->getForecastPeriod(),
                    $forecast->getWeather(), $forecast->getWeatherURL(),
                    $forecast->getWindSpeedInMPH(), $forecast->getWindDirection(),
                    $forecast->getWindURL(), $forecast->getSnowExpectedInCentimeters(),
                    $forecast->getMinTemperature(), $forecast->getMaxTemperature(),
                    $forecast->getFeelsLikeTemperature());
                $wpdb->query($sql);
            }
            $sql = "COMMIT;\n";
            $wpdb->query($sql);
        }
    }

    /**
     * Called for every forecast for this resort. Determine whether 3-day or 6-day forecast need to be updated.
     *
     * @param DateTime $now
     * @param DateTime $periodEnd
     * @param float $freshSnowfallInCentimeters
     * @return void
     */
    private function addSnow(DateTime $now, DateTime $periodEnd, float $freshSnowfallInCentimeters): void
    {
        $dayDifference = $now->diff($periodEnd)->days;
        if ($dayDifference < 0) {
            throw new InvalidArgumentException("Period End is before Now");
        }
        if ($dayDifference < 3) {
            $this->fltSnowForecastNext3DaysInCentimeters += $freshSnowfallInCentimeters;
           }
        if ($dayDifference < 6) {
            $this->fltSnowForecastNext6DaysInCentimeters += $freshSnowfallInCentimeters;
        }
    }
}
