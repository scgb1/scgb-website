<?php
declare(strict_types=1);
namespace SCGB;

use DateTime;
use Exception;

/**
 * Class to hold Resort Data. This is initially populated from a CSV and then enriched with elevation data.
 */
class ResortData
{
    /**
     * Properties containing data loaded from the SkiClub file - The column headers must match these names.
     */
    protected int $intSCGBResortID = 0;             // Mandatory
    protected string $strResortName = '';           // Mandatory
    protected string $strResortCountry = '';        // Mandatory
    public ?int $intResortSkiServiceID = null;      // Optional - Data not always available - NULL if not set
    public float $fltResortLongitude = 0.0;      // Mandatory
    public float $fltResortLatitude = 0.0;       // Mandatory
    public float $fltMountainLongitude = 0.0;    // Mandatory
    public float $fltMountainLatitude = 0.0;     // Mandatory

    /**
     * Added after initial load from Resort File.
     *
     * If available, se Elevations cached locally in a file else attempt to get them from Google
     */
    public ?float $intResortAltitude = null;
    public ?float $intMountainAltitude = null;

    /**
     * Data from Ski Service - all optional and null if not set/available
     */
    protected ?string $strSkiServiceResortName = null;
    protected ?int $intSnowLowerPisteInCentimeters = null;
    protected ?int $intSnowUpperPisteInCentimeters = null;
    protected ?DateTime $dtmLastSnowed = null;
    protected ?int $intLiftsOpen = null;
    protected ?int $intLiftsTotal = null;
    protected ?string $strOffPisteSnowConditions = null;
    protected ?DateTime $dtmResortOpening = null;
    protected ?DateTime $dtmResortClosing = null;

    /**
     * Used to check for duplicates - not exposed outside the class
     */
    private static array $arrResortDataByID = array(); // array of intSCGBResortID
    /**
     * Array of valid forecasts for the next 10 days
     */

    /**
     * Constructor - takes an associated array containing a line from the Resort Data CSV.
     *
     * The config item 'resortDataFile' points at the CSV
     *
     * @param $row - associative array with row level data from the CSV
     * @returns - the new object
     * @throws Exception
     */
    public function __construct(array $row)
    {
        // There could be a lot of other data in the array but these the ones required.
        // only intSCGBResortID, strResortName and strResortCountry mandatory
        // The others set to NULL if not present or cant be converted
        // TODO Handle Missing Data Better
        $this->intSCGBResortID = ResortData::getrowvalue(
            $row,
            'intSCGBResortID',
            'int',
            true
        );
        $this->strResortCountry = ResortData::getrowvalue(
            $row,
            'strResortCountry',
            'string',
            true
        );
        $this->strResortName = ResortData::getrowvalue(
            $row,
            'strResortName',
            'string',
            true
        );
        $this->intResortSkiServiceID = ResortData::getrowvalue(
            $row,
            'intResortSkiServiceID',
            'int',
            false
        );
        $this->fltResortLatitude = ResortData::getrowvalue(
            $row,
            'fltResortLatitude',
            'float',
            true
        );
        $this->fltResortLongitude = ResortData::getrowvalue(
            $row,
            'fltResortLongitude',
            'float',
            true
        );
        $this->fltMountainLatitude = ResortData::getrowvalue(
            $row,
            'fltMountainLatitude',
            'float',
            true
        );
        $this->fltMountainLongitude = ResortData::getrowvalue(
            $row,
            'fltMountainLongitude',
            'float',
            true
        );

        // Check whether this is not a dupe and check whether the lat/longs valid
        $this->checkDataValidity();

        return $this;
    }

    /** @noinspection PhpUnused */
    public function getResortSkiServiceID(): int|null
    {
        return $this->intResortSkiServiceID;
    }

    /**
     * static method - Load the base resort information from a CSV and then add Elevation Data.
     *
     * @return array - array of WeatherData objects
     * @throws Exception
     */
    public static function getResortData(string $strDataFile): array
    {
        // Map lines of the string returned by file function to $rows array.
        $rows = array_map('str_getcsv', file($strDataFile));
        Common::logger()->debug(
            "Loading resort data",
            array(
                'data file' => $strDataFile,
                'rows' => count($rows),
                'file' => basename(__FILE__),
                'function' => __FUNCTION__,
                'line' => __LINE__,
            )
        );

        //Get the first row that is the HEADER row.
        $header_row_tmp = array_shift($rows);

        // Need to remove non printables - Weird side effect from saving excel as a csv
        $i = 0;
        $header_row = array();
        foreach ($header_row_tmp as $ignored) {
            $header_row[$i] = preg_replace('/[[:^print:]]/', '', $header_row_tmp[$i++]);
        }

        //This array holds the final response - which is keyed on the first column of the header
        $arrResortData = [];

        foreach ($rows as $row) {
            $tmpResortData = null;
            $tmpResortData['intSCGBResortID'] = $row[0];
            for ($i = 1; $i < count($row); $i++) {
                $tmpResortData[$header_row[$i + 1]] = $row[$i];
            }
            $classResortData = new ResortData($tmpResortData);
            $arrResortData[$classResortData->intSCGBResortID] = $classResortData;
        }

        ResortData::addElevationData($arrResortData);
        return $arrResortData;
    }

    /**
     *  function to check the validity of the data in the object
     * - Mandatory fields present
     * - Fields of the correct type
     * - Fields within the correct range
     * - Duplicate of an existing record?
     *
     * @return void
     * @throws Exception
     */
    private function checkDataValidity(): void
    {
        // check $intSCGBResortID is positive and not a dupe
        if ($this->intSCGBResortID <= 0) {
            throw new Exception("Invalid SCGBResortID " . $this->intSCGBResortID . "/" . $this->strResortName);
        }
        if (array_key_exists($this->intSCGBResortID, self::$arrResortDataByID)) {
            throw new Exception("Duplicate SCGBResortID " . $this->intSCGBResortID . "/" . $this->strResortName);
        }
        self::$arrResortDataByID[$this->intSCGBResortID] = $this;

        // Check Latitude and Longitude are valid
        if ($this->fltResortLatitude != null && ($this->fltResortLatitude < -90 || $this->fltResortLatitude > 90)) {
            throw new Exception("Invalid Resort Latitude -> " . $this->strResortName . "/" . $this->fltResortLatitude);
        }
        if ($this->fltResortLongitude != null && ($this->fltResortLongitude < -180 ||
                $this->fltResortLongitude > 180)) {
            throw new Exception("Invalid Resort Longitude " . $this->strResortName . "/" . $this->fltResortLongitude);
        }
        if ($this->fltMountainLatitude != null && ($this->fltMountainLatitude < -90 ||
                $this->fltMountainLatitude > 90)) {
            throw new Exception("Invalid Mountain Latitude " . $this->strResortName . "/" . $this->fltMountainLatitude);
        }
        if ($this->fltMountainLongitude != null && ($this->fltMountainLongitude < -180 ||
                $this->fltMountainLongitude > 180)) {
            throw new Exception("Invalid Mountain Longitude " . $this->strResortName . "/" .
                $this->fltMountainLongitude);
        }

        // check the $dtmResortClosing is after $dtmResortOpening
        if ($this->dtmResortClosing != null && $this->dtmResortOpening != null) {
            if ($this->dtmResortClosing < $this->dtmResortOpening) {
                throw new Exception("Resort Closing Date is before Resort Opening Date " . $this->strResortName);
            }
        }
    }

    /**
     * Extract a names value which can be used in a SQL statement.
     *
     * @param array $row
     * @param string $key
     * @param string $type
     * @param bool $mandatory
     * @return string|int|float|null
     * @throws Exception
     */
    private static function getrowvalue(
        array  $row,
        string $key,
        string $type,
        bool   $mandatory
    ): string|int|float|null {

        if (array_key_exists($key, $row)) {
            $retVal = $row[$key];
            if ($retVal == '' || $retVal == '-') {
                $retVal = null;
            }
        } else {
            if ($mandatory) {
                throw new Exception("Key " . $key . " not found in row");
            } else {
                $retVal = null;
            }
        }

        // if $retVal is not null then check it is of the correct type - if not return NULL
        if ($retVal) {
            if ($type == 'string') {
                if (!is_string($retVal)) {
                    $retVal = null;
                }
            } elseif ($type == 'int') {
                try {
                    $retVal = intval($retVal);
                } catch (Exception) {
                    $retVal = null;
                }
            } elseif ($type == 'float') {
                try {
                    $retVal = floatval($retVal);
                } catch (Exception) {
                    $retVal = null;
                }
            } else {
                throw new Exception("Unknown type " . $type);
            }
        }
        return $retVal;
    }

    /**
     * Static function to add elevation data to the resort data.
     *
     * @param array $arrResortData - array of WeatherData objects
     * @return void
     * @throws Exception
     */
    private static function addElevationData(array $arrResortData): void
    {
        Common::logger()->debug(
            "Adding elevation data to resort data",
            array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,)
        );

        // Loop through the resorts and add the elevation data - we have to iterate twice as we want to avoid making
        // multiple calls to the API
        foreach ($arrResortData as $resortData) {
            ElevationData::checkLatLong($resortData->fltResortLatitude, $resortData->fltResortLongitude);
            ElevationData::checkLatLong($resortData->fltMountainLatitude, $resortData->fltMountainLongitude);
        }

        ElevationData::getMissingData();

        // Now add the elevation data to the resort data
        foreach ($arrResortData as $resortData) {
            $resortData->intResortAltitude = ElevationData::getElevation(
                $resortData->fltResortLatitude,
                $resortData->fltResortLongitude
            );
            $resortData->intMountainAltitude = ElevationData::getElevation(
                $resortData->fltMountainLatitude,
                $resortData->fltMountainLongitude
            );
        }
    }

    /**
     * Convert the passed parameter to something suitable for insertion in the database.
     * If passed value is null then return NULL, else return the value surrounded by single quotes if appropriate
     *
     * @param mixed $value - the value to be converted
     * @return string
     */
    private static function getSQLValue(mixed $value): string
    {
        if ($value === null) {
            return 'NULL';
        } else {
            if (is_numeric($value)) {
                return strval($value);
            } elseif (is_a($value, 'DateTime')) {
                return "'" . $value->format('Y-m-d H:i:s') . "'";
            } else {
                return "'" . $value . "'";
            }
        }
    }

    public static function getResortDataByID(int $intSCGBResortID): ?ResortData
    {
        if (array_key_exists($intSCGBResortID, self::$arrResortDataByID)) {
            return self::$arrResortDataByID[$intSCGBResortID];
        } else {
            return null;
        }
    }
}
