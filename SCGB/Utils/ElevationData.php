<?php
declare(strict_types=1);
namespace SCGB;

use Exception;

/**
 * Container used to manage elevation data from Google.
 *
 * Elevation data is not directly used by the website, but we need it to help confirm that the correctness of the
 * coordinates of the forecast locations.
 *
 * The elevation data is cached in a local file and then any missing data is fetched from Google
 */
class ElevationData
{
    const ELEVATION_URL = 'https://maps.googleapis.com/maps/api/elevation/json?key=%s&locations=%s';

    /**
     * Array keyed on  '[lat], [long]' to 4 dp. Initially loaded from a file and then added to as we go along
     */
    private static array $elevationData = array();

    /**
     * Array of points to get from Google. Keyed on '[lat],[long]' to 4 dp.
     */
    private static array $elevationsToGet = array();

    /**
     * See if we already have the elevation data for this point. If not add it to the list of points to get later.
     *
     * @param float|null $longitude
     * @param float|null $latitude
     * @return void
     * @throws Exception
     */
    public static function checkLatLong(float|null $latitude, float|null $longitude) : void
    {
        // First check both not null else we can't do anything
        if ($longitude === null || $latitude === null) {
            return;
        }

        // Load Any elevation data from the file pointed to by the config item elevationDataFile
        if (count(self::$elevationData) == 0 && count(self::$elevationsToGet) == 0) {
            $elevationDataFile = $GLOBALS['argv'][2];

            if ($elevationDataFile && file_exists($elevationDataFile)) {
                self::$elevationData = json_decode(file_get_contents($elevationDataFile), true);

                Common::logger()->debug(
                    "Loaded " . count(self::$elevationData) . " elevation data points from $elevationDataFile",
                    array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,)
                );
            }
        }

        // Check if this point exists in the cached data
        $latlong = self::standardPrecision($latitude) . "," . self::standardPrecision($longitude);
        if (!isset(self::$elevationData[$latlong])) {
            // We don't have the elevation data for this point so add it to the list of points to get
            Common::logger()->debug(
                "Elevation Data not found for lat-> $latitude, long -> $longitude, " .
                "- fetching from Google",
                array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,)
            );
            self::$elevationsToGet[] = array($latlong);
        }
    }

    /**
     * Returns the elevation of the given point - at this point it is assumed that this already loaded.
     *
     * @param float|null $latitude
     * @param float|null $longitude
     * @return float|null
     * @noinspection PhpUnused
     * @throws Exception
     */
    public static function getElevation(float|null $latitude, float|null $longitude) : float|null
    {
        // First check both not null else we can't do anything
        if ($longitude === null || $latitude === null) {
            return null;
        }

        $latlong = self::standardPrecision($latitude) .",". self::standardPrecision($longitude);
        if (array_key_exists($latlong, self::$elevationData)) {
            return self::$elevationData[$latlong];
        }
        Common::logger()->error(
            "Elevation Data not found for lat -> $latitude, long -> $longitude",
            array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,)
        );
        return null;
    }

    /**
     * If new locations then get the elevation data for them from Google and add it to the existing data
     * and save it to the cache file.
     *
     * @return void
     * @throws Exception
     * @noinspection PhpUnused
     */
    public static function getMissingData() : void
    {
        // If we have any points to get then get them
        if (count(self::$elevationsToGet) > 0) {
            // Remove Duplicates
            self::$elevationsToGet = array_unique(self::$elevationsToGet, SORT_REGULAR);
            $elevationData = ElevationData::getGoogleElevationData(self::$elevationsToGet);
            // Merge the new data into the existing data
            self::$elevationData = self::$elevationData + $elevationData;

            // Save the new data to the file
            $elevationDataFile = $GLOBALS['argv'][2];
            if ($elevationDataFile) {
                Common::logger()->debug(
                    "Saving " . count(self::$elevationData) . " elevation data points to $elevationDataFile",
                    array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,)
                );
                file_put_contents($elevationDataFile . ".json", json_encode(self::$elevationData));

                // Write out as CSV as well
                $fp = fopen($elevationDataFile . ".csv", 'w');
                fwrite($fp, "latitude,longitude,elevation\n");
                foreach (self::$elevationData as $latlong => $elevation) {
                    fwrite($fp, "$latlong,$elevation\n");
                }
                fclose($fp);
            }
        }
    }

    /**
     * Fetch a set of elevation Data from the Google API.
     *
     * @param array $LongLatPairs - array of comma separated long/lat pairs
     * @return array - array of elevation data keyed on the lat, long
     * @throws Exception
     */
    private static function getGoogleElevationData(array $LongLatPairs) : array
    {
        Common::logger()->debug(
            "Fetching " . count($LongLatPairs) . " elevation data points from Google",
            array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,)
        );
        $arrElevations = array();

        for ($ws = 0; $ws < count($LongLatPairs); $ws+=10) {
            // Split the array into chunks of 10
            // It should be able to handle 512, but it returns inconsistent results asked for too many.
            $arrElevationsChunk = array_slice($LongLatPairs, $ws, 10);

            // Now get both forecasts and combine them
            $strWeatherURL = ElevationData::getElevationURL($arrElevationsChunk);
            $jsonGoogleElevations = Common::curl($strWeatherURL);
            $arrGoogleElevations = json_decode($jsonGoogleElevations, true)['results'];
            foreach ($arrGoogleElevations as $elevationData) {
                $long = ElevationData::standardPrecision($elevationData['location']['lng']);
                $lat = ElevationData::standardPrecision($elevationData['location']['lat']);
                $arrElevations[$lat . "," . $long] = intval($elevationData['elevation']);
            }
        }
        return $arrElevations;
    }

    /**
     * Create a URL for Google with a list of locations to get elevation data for.
     *
     * @param array $arrElevations
     * @return string
     * @throws Exception
     */
    private static function getElevationURL(array $arrElevations) :string
    {
        $strElevationList = "";
        $googleApiKey = $GLOBALS['argv'][3];
        // Create a | separated list of Lat/Long pairs
        for ($ws = 0; $ws < count($arrElevations); $ws++) {
            $strElevationList .= $arrElevations[$ws][0] . '|';
        }
        // remove the trailing |
        $strElevationList = substr($strElevationList, 0, -1);

        // now URL encode and return a formatted string
        return sprintf(ElevationData::ELEVATION_URL, $googleApiKey, urlencode($strElevationList));
    }

    /**
     * All co-ordinates to 4 decimal places. This gives a precision of ~+/- 10m so more than enough for our purposes.
     *
     * @param $value
     * @return string
     */
    public static function standardPrecision($value) : string
    {
        return strval(round($value, 4));
    }
}
