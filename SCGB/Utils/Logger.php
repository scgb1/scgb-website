<?php

namespace SCGB;


class Logger
{
    const SCGB_LOG_LEVEL = array(
        'EMERGENCY' => 0,
        'ERROR' => 1,
        'WARNING' => 2,
        'INFO' => 3,
        'DEBUG' => 4
    );
    private int $log_level;
    private $file_handle = null;
    private int $print_to_console;

    public function __construct(string $script_name, bool $command_line = false)
    {
        $this->print_to_console = $command_line;
        if (!isset($_ENV['SCGB_LOG_LEVEL'])) {
            $_ENV['SCGB_LOG_LEVEL'] = 'INFO';
        }
        if (!key_exists(strtoupper($_ENV['SCGB_LOG_LEVEL']), self::SCGB_LOG_LEVEL)) {
            error_log('Invalid log level ' . $_ENV['SCGB_LOG_LEVEL'] . ' specified in environment variable SCGB_LOG_LEVEL');
            $_ENV['SCGB_LOG_LEVEL'] = 'INFO';
        }
        $this->log_level = self::SCGB_LOG_LEVEL[strtoupper($_ENV['SCGB_LOG_LEVEL'])];

        if (isset($_ENV['SCGB_LOG_DIR'])) {
            if (!file_exists($_ENV['SCGB_LOG_DIR'])) {
                mkdir($_ENV['SCGB_LOG_DIR'], 0700, true);
            }

            // Construct the log file name based on date time and script name
            $log_file_name = $_ENV['SCGB_LOG_DIR'] . '/' . date('Ymd-His') . '-' . $script_name . '.log';

            // Open the log file for appending - just in case there is already a log file with the same name
            $this->file_handle = fopen($log_file_name, 'a');
            if ($this->file_handle === false) {
                error_log('Unable to open log file ' . $log_file_name);
            }
        }
    }

    public function emergency($message, $context = array()) : void
    {
        $this->log('EMERGENCY', $message, $context);
    }
    public function error($message, $context = array()) : void
    {
        $this->log('ERROR', $message, $context);
    }
    public function warning($message, $context = array()) : void
    {
        $this->log('WARNING', $message, $context);
    }
    public function info($message, $context = array()) : void
    {
        $this->log('INFO', $message, $context);
    }
    public function debug($message, $context = array()) : void
    {
        $this->log('DEBUG', $message, $context);
    }

    private function log($level, $message, $context = array()) : void
    {
        if (!$this->file_handle) {
            return;
        }
        if (self::SCGB_LOG_LEVEL[$level] > $this->log_level) {
            return;
        }
        $message = $this->formatMessage($level, $message, $context);
        fwrite($this->file_handle, $message . "\n");
        if ($this->print_to_console) {
            echo $message . "\n";
        }
    }

    private function formatMessage(string $level, $message, $context = array()) : string
    {
        $message = '[' . date('Y-m-d\TH:i:s') . '] ' .
            $level . ': ' . $message;
        if (count($context) > 0) {
            $message .= ' ' . json_encode($context);
        }
        return $message;
    }
}
