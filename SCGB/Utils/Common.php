<?php
declare(strict_types=1);
namespace SCGB;

use Dotenv\Dotenv;
use Exception;
use DateTime;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\HandlerStack;
use GuzzleRetry\GuzzleRetryMiddleware;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

require_once __DIR__ . '/scgb_autoload.php';
require_once __DIR__ . '/../../vendor/autoload.php';

/**
 * Utility functions - particularly logging
 * Also set up the WordPress environment if running in Debug
 */
class Common
{
    private static string $function_name;
    private static DateTime $start_time;
    private static ?Logger $logger = null;
    private static bool $command_line = false;
    private static HandlerStack $stack;
    public function __construct()
    {
        return $this;
    }

	/**
	 * Initialise the application. Setup logging, read the config file and process command line overrides.
	 * @throws Exception
	 */
    static public function scgb_initialise(string $function_name): void
    {
        self::$start_time = new DateTime();
        // if the function name contains a namespace, then strip it off
        if (str_contains($function_name, '\\'))
            self::$function_name = substr($function_name, strrpos($function_name, '\\') + 1);
        else
            self::$function_name = $function_name;

        // Set up Guzzle Retry support
        self::$stack = HandlerStack::create();
        self::$stack->push(GuzzleRetryMiddleware::factory());

        // Establish if directly called by WordPress or command line
        self::normalise_environment();

        // Load up Dotenv
        $dotenv = Dotenv::createImmutable(ABSPATH);
        $dotenv->load();

        // Basic initialisation, including setting up logging. Until this is completed, the code can only exit
        try {
            self::$logger = new Logger(self::$function_name, self::$command_line);

            self::logger()->info('Function: ' . $function_name . ' - Starting',
				['function' => __FUNCTION__, 'file' => basename(__FILE__),  'line' => __LINE__]);
            self::healthchecks('start');

        } catch (Exception $e) {
            error_log('Failed to perform basic initialisation - Cannot continue: ' . $e->getMessage() . "\n");
            throw $e;
        }
    }

	/**
	 * @throws Exception
	 */
	static public function scgb_finalise(int $status = 0): void
	{
        wp_cache_flush();
        // Calculate the execution time in seconds
        $end_time = new DateTime();
        $execution_time = $end_time->getTimestamp() - self::$start_time->getTimestamp();

		self::logger()->info('Function: ' . self::$function_name . ' - Completed.  Execution time: ' .
            $execution_time . ' seconds',
            ['function' => __FUNCTION__, 'file' => basename(__FILE__),  'line' => __LINE__]);
		self::healthchecks(strval($status));
        if ($status == 0) {
            self::betterstack_heartbeat();
        }
	}

	/**
	 *  Called directly by WordPress or from the command line?
	 */
	static private function normalise_environment(): void
	{
		// Do the functions get_option and add_action exist? If not, then we called from the command line
		if (!function_exists('get_option')) {
			// Called from the command line. Need to call wp-load.php
			// Find the wp-load.php file
            self::$command_line = true;
			$wp_load_file = self::find_wordpress();
			if ($wp_load_file === null) {
				wp_die("Failed to find wp-load.php file.  Can't continue");
			}
			// Call wp-load.php
			require_once($wp_load_file);
		}

		// Do some paranoid checks:
		// - is the function get_option defined?
		// - variables ABS_PATH and DB_NAME defined?
		// - a readable file called .env in the root directory?
		if (!function_exists('get_option') ||
			!defined('ABSPATH') ||
			!defined('DB_NAME') ||
			!file_exists(ABSPATH . '.env') ||
			!is_readable(ABSPATH . '.env')) {
			wp_die("Failed to initialise correctly.  Can't continue\n");
		}
	}

	/**
	 * Find the wp-load.php file - There needs to be env var ABSPATH which points to the root of the WordPress
	 */
	static private function find_wordpress(): string|null
	{
		// Check env var ABSPATH is set
		$abs_path = getenv('ABSPATH');
		if ($abs_path === false) {
			return null;
		}

		$wp_load_file = $abs_path . '/wp-load.php';
		if (!file_exists($wp_load_file)) {
			return null;
		}
		return $wp_load_file;
	}

	/**
	 * @throws Exception
     */
	static private function healthchecks(string $status): void
	{
		// See if a file of healthchecks URLS exists ABSPATH . '.healthchecks.csv'
		$healthchecks_file = ABSPATH . '.healthchecks.csv';

		// If the file exists, look for a URL for the current function. File format is function,url
		if (file_exists($healthchecks_file)) {
			$healthchecks = file($healthchecks_file, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
			foreach ($healthchecks as $healthcheck) {
				$healthcheck = explode(',', $healthcheck);
				if ($healthcheck[0] == self::$function_name) {
					// Found a healthcheck URL for this function
					$healthcheck_URL = $healthcheck[1];
					// Call the URL
					self::curl( $healthcheck_URL . '/' . $status);
                    break;
				}
			}
		}

	}

    /**
     * @throws Exception
     */
    static private function betterstack_heartbeat(): void
    {
        if (!key_exists('BETTERSTACK_API_ROOT', $_ENV)) {
            return;
        }
        // See if a file of betterstack heartbeat keys URLS exists ABSPATH . '.betterstack.csv'
        $betterstack_file = ABSPATH . '.betterstack.csv';

        // If the file exists, look for a URL for the current function. File format is function,url
        if (file_exists($betterstack_file)) {
            $betterstack = file($betterstack_file, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
            foreach ($betterstack as $betterstack_data) {
                $betterstack_data = explode(',', $betterstack_data);
                if ($betterstack_data[0] == self::$function_name) {
                    // Found a healthcheck URL for this function
                    $betterstack__key = $betterstack_data[1];
                    // Call the URL
                    self::curl( $_ENV['BETTERSTACK_API_ROOT'] . '/' . $betterstack__key);
                    break;
                }
            }
        }
    }

    /**
     * Listen for retry events
     *
     * @param int $attemptNumber How many attempts have been tried for this particular request
     * @param float $delay How long the client will wait before retrying the request
     * @param RequestInterface $request Request
     * @param array $options Guzzle request options
     * @param ResponseInterface|null $response Response (or NULL if response not sent; e.g. connect timeout)
     * @throws Exception
     */
    public static function timeout_report(int   $attemptNumber, float $delay, RequestInterface $request,
                                          array &$options, ?ResponseInterface $response)
    {
        $response_status = $response === null ? 'NULL' : $response->getStatusCode();
        $message = "Retrying request to " . $request->getUri()->getPath() .
            ".  Server responded with " . $response_status .
            ". Will wait ".  number_format($delay, 2) .
            " seconds.  This is attempt #" . $attemptNumber;

        self::logger()->warning(
            $message,
            ['function' => __FUNCTION__, 'file' => basename(__FILE__),  'line' => __LINE__]
        );
}

    static public function curl($url, $token = null): string
    {
        try {
            $client = new Client([
                'base_uri' => $url,
                'handler' => self::$stack,
                'retry_on_timeout' => true,
                'timeout' => 600,
                'retry_on_status' => [429, 500, 502, 503, 504],
                'on_retry_callback' => [self::class, 'timeout_report']
            ]);

            if ($token != null) {
                $response = $client->request('GET', $url,
                    ['headers' => ['Authorization' => 'Bearer ' . $token]]
                );
            } else {
                $response = $client->request('GET', $url);
            }
        } catch (GuzzleException $e) {
            try {
                self::logger()->error('GuzzleException: ' . $e->getMessage(),
                    array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,));
            } catch (Exception $e) {
                wp_die('Exception: ' . $e->getMessage());
            }
            return '';
        }

        return $response->getBody()->getContents();
    }

    /**
     * Returns a timestamped filename and creates the target directory if it doesn't exist.
     *
     * @param string $basename - the filename to timestamp
     * @return string
     */
    static public function getTimeStampedFilename(string $basename): string
    {
        $dirname = dirname($basename);
        $filename = basename($basename);
        if (!file_exists($dirname)) {
            mkdir($dirname, 0700, true);
        }

        // Add a timestamp to filename
        $filename = date('Ymd.His') . '-' . $filename;
        return ($dirname . '/' . $filename);
    }

    static public function logger() : Logger
    {
        // If the logger not already set up, then in trouble!
        if (self::$logger === null) {
            error_log('EMERGENCY: ' . __FILE__ . ' ' . __LINE__ . ' ' . __FUNCTION__ . ' ' . 'Logger not set up');
            wp_die(__FUNCTION__ . "logger not set up - can't continue");
        }
        return self::$logger;
    }


	public static function update_post_menu_order(array $post_id_array) : void
	{
        global $wpdb;
		foreach ($post_id_array as $menu_order => $ID) {
			$sql = "UPDATE wp_posts SET menu_order = " . $menu_order . " WHERE ID = " . $ID;
            $wpdb->query($sql);
		}
	}
}
