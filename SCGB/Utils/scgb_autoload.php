<?php

/**
 * Simple Autoloader for SCGB Classes … Just search down the directory tree for the class file
 * ignore the directory vendor
 */

spl_autoload_register('scgb_autoload');
function scgb_autoload($class_name) : void
{
    // Start at the script directory
    $dir = dirname($_SERVER['SCRIPT_NAME']) . '/SCGB';

    // Split the class name into its parts
    $arrParts = explode('\\', $class_name);

    if (count($arrParts) != 2 || $arrParts[0] != 'SCGB') {
        return;
    }

    $file = $arrParts[1] . '.php';
    $search_base = __DIR__ . '/../';

    // Do a recursive search from here down
    $search = find_file($search_base, $file);

    if ($search !== null) {
        require_once $search;
    }
}

function find_file(string $dir, string $file_to_search) : ?string
{
    $files = scandir($dir);
    $found = null;
    foreach($files as $value)
    {
        $path = realpath($dir.DIRECTORY_SEPARATOR.$value);
        if(!is_dir($path)) {
            if($file_to_search == $value)
            {
                $found = $path;
                break;
            }

        } elseif ($value != "." && $value != "..")
        {
            $found = find_file($path, $file_to_search);
            if ($found !== null) {
                break;
            }
        }
    }
    return $found;
}