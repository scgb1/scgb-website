<?php
declare(strict_types=1);
namespace SCGB;

use Exception;

/**
 * Container used to fetch forecast data from CollectorDTN
 */
class CollectorDTN extends Collector
{
    private bool $useV1API = false;
    private ?string $dtnToken = null;

    /**
     * Constructor.
     *
     * Depending on whether:
     * - using legacy or V1
     * - saving or loading data from a file
     * Set the appropriate properties and collect a CollectorDTN token if necessary
     *
     * @throws Exception
     */
    public function __construct()
    {
        // legacy or V1 API
        $strAPIVersion = 'Legacy';
        $weather_client_audience = $_ENV['WEATHER_CLIENT_AUDIENCE'] ?? null;
        if ( $weather_client_audience !== null) {
            $this->useV1API = true;
            $strAPIVersion = 'V1';
        }

        Common::logger()->debug(
            "Using CollectorDTN API Version $strAPIVersion",
            array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__, )
        );

        parent::__construct($strAPIVersion);

        // If not loading from files then get the CollectorDTN Token
        if ($this->loadForecastDataFromDir === null) {
            Common::logger()->debug(
                "Loading data from CollectorDTN - Collect a CollectorDTN Token",
                array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,)
            );
            $this->dtnToken = CollectorDTN::getDTNLogonToken();
        }
        return $this;
    }

    /**
     * Get a forecast for a location from CollectorDTN.
     *
     * To maintain compatibility with the new API only we only request 1 location at a time.
     * The old API allowed multiple locations to be requested in a single call!
     * Also, because not all parameters available in the same call we may make multiple calls to get all the data
     *
     * @param float $latitude
     * @param float $longitude
     * @return array
     * @throws Exception
     */
    public function getForecastForLocation(float $latitude, float $longitude): array
    {
        Common::logger()->debug(
            'Getting Forecast from CollectorDTN for ' . $latitude . '/' . $longitude,
            array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,)
        );

        if ($this->useV1API) {
            $forecast = new ForecastDTNV1();
        } else {
            $forecast = new ForecastDTNLegacy();
        }
        return $this->getForecastForLocationFromProvider($latitude, $longitude, $forecast, $this->dtnToken);
    }

    /**
     * Create a URL to fetch a forecast from CollectorDTN. This differs depending on the API version.
     *
     * @param float $latitude
     * @param float $longitude
     * @return array
     * @throws Exception
     */
    protected function getForecastURLs(float $latitude, float $longitude): array
    {
        if ($this->useV1API) {
            return ForecastDTNV1::getForecastURLs($latitude, $longitude);
        } else {
            return ForecastDTNLegacy::getForecastURLs($latitude, $longitude);
        }
    }

    /**
     * Get the CollectorDTN logon token - the V1 and legacy APIs have different methods of getting this
     *
     * @return string | null - the token or null if we didn't get it
     * @throws Exception
     */
    private function getDTNLogonToken(): string|null
    {
        Common::logger()->debug(
            'Getting CollectorDTN Logon Token',
            array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,)
        );

        $client_id = $_ENV['DTN_CLIENT_ID'];
        $client_secret = $_ENV['DTN_CLIENT_SECRET'];

        if (empty($client_id) || empty($client_secret)) {
            Common::logger()->emergency(
                'Error getting CollectorDTN Logon Token: DTN_CLIENT_ID or DTN_CLIENT_SECRET not set',
                array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,)
            );
            return null;
        }

        $ch = curl_init();

        // This the point where the code where we determine whether using the legacy or V1 API
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);

        if ($this->useV1API) {
            $client_audience = Common::getConfigItem('weatherClientAudience');
            curl_setopt($ch, CURLOPT_URL, ForecastDTNV1::getAuthURL());
            curl_setopt($ch, CURLOPT_ENCODING, '');
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
            curl_setopt(
                $ch,
                CURLOPT_POSTFIELDS,
                '{
            "grant_type": "client_credentials",
            "client_id": "' . $client_id . '",
            "client_secret": "' . $client_secret . '",
            "audience": "' . $client_audience . '"
        }'
            );
            curl_setopt(
                $ch,
                CURLOPT_HTTPHEADER,
                array(
                    'Accept: application/json',
                    'Content-Type: application/json'
                )
            );
        } else {
            $comb = $client_id . ':' . $client_secret;
            curl_setopt($ch, CURLOPT_URL, ForecastDTNLegacy::getAuthURL());
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Content-Type: application/x-www-form-urlencoded',
            ]);
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_USERPWD, $comb);
            curl_setopt($ch, CURLOPT_POSTFIELDS, 'grant_type=client_credentials');
        }
        $response = curl_exec($ch);

        if (curl_errno($ch)) {
            Common::logger()->error(
                'Error getting CollectorDTN Logon Token: ' . curl_error($ch),
                array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,)
            );
            return null;
        }

        $auth_string = json_decode($response, true); // token will be with in this json
        if ($this->useV1API) {
            $auth_string = $auth_string['data'];
        }
        Common::logger()->debug(
            "Successfully Connected to CollectorDTN",
            array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,)
        );

        return ($auth_string["access_token"]);
    }
}
