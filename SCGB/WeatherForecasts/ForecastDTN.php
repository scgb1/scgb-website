<?php

namespace SCGB;

use Exception;

class ForecastDTN extends Forecast
{
    /**
     * @inheritDoc
     */
    public static function getForecastsForLocation(array $arrForecastsFromProvider): array
    {
        //NOOP - not implemented
        throw new Exception("shouldn't be here!");
    }

    /**
     * @return string
     * @throws Exception
     */
    public static function getAuthURL(): string
    {
        //NOOP - not implemented
        throw new Exception("shouldn't be here!");
    }

    /**
     * @param string|null $period
     * @return string
     * @throws Exception
     */
    public static function getWeatherCodes(string $period = null): string
    {
        //NOOP - not implemented
        throw new Exception("shouldn't be here!");
    }

    /**
     * @inheritDoc
     */
    public static function getForecastURLs(float $latitude, float $longitude): array
    {
        //NOOP - not implemented
        throw new Exception("shouldn't be here!");
    }

    /**
     * @inheritDoc
     */
    protected function setWeather(): void
    {
        //NOOP - not implemented
        throw new Exception("shouldn't be here!");
    }

    /**
     * @inheritDoc
     */
    protected function setWeatherURL(): void
    {
        //NOOP - not implemented
        throw new Exception("shouldn't be here!");
    }
}
