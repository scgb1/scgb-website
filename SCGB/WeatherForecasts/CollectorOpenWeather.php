<?php
declare(strict_types=1);
namespace SCGB;

use Exception;

/**
 * Class CollectorOpenWeather
 * @package WP
 */

class CollectorOpenWeather extends Collector
{
    private string $apiKey;

    /**
     * @throws Exception
     */
    public function __construct()
    {
        try {
            $this->apiKey = Common::getConfigItem('openWeatherAPIKey');
        } catch (Exception $e) {
            Common::logger()->emergency(
                'Failed to get OpenWeather API Key: ' . $e->getMessage(),
                array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__)
            );
            throw $e;
        }

        parent::__construct('OW');
        return $this;
    }

    /**
     * @throws Exception
     */
    public function getForecastForLocation(float $latitude, float $longitude): array
    {
        Common::logger()->debug(
            'Getting Forecast from OpenWeather for ' . $latitude . '/' . $longitude,
            array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,)
        );

        $forecast = new ForecastOpenWeather();
        return $this->getForecastForLocationFromProvider($latitude, $longitude, $forecast);
    }

    /**
     * @throws Exception
     */
    protected function getForecastURLs(float $latitude, float $longitude): array
    {
        // Get the URLa and add the app id onto the end
        $forecastURLs = array();
        foreach (ForecastOpenWeather::getForecastURLs($latitude, $longitude) as $url) {
            $forecastURLs[] = $url . '&appid=' . $this->apiKey;
        }
        return $forecastURLs;
    }
}
