<?php

declare(strict_types=1);

require_once __DIR__ . '/../SCGB/Utils/scgb_autoload.php';

use SCGB\Common;
use SearchWP\Statistics;

/**
 * Keep the popular searches list current
 * Get recent stats from WP_Search and then select 5 at random from the top 15
 */
function scgb_daily_update_search_statistics() : void
{
    $status = 0 ;
    try {
        Common::scgb_initialise(__FUNCTION__);

        wp_cache_flush();
        $x = get_option('popular_searches');
        $temp = get_option('scgb_popular_searches_temp');

        // Temp Save during release
        if (!$temp) {
            if (!update_option('scgb_popular_searches_temp', $x))
            {
                wp_die('Failed to update option scgb_popular_searches_temp');
            }
        }
        // Get Search WP Stats
        $statistics = Statistics::get_popular_searches([
            'days' => 7,
            'engine' => 'default',
            'limit' => 1000,
        ]);

        // Iterate through and combine terms which have + in them - replace with a space
        // also merge lower and upper case
        $cleaned_statistics = array();
        foreach ($statistics as $statistic) {
            $query = $statistic->query;
            $count = $statistic->searches;
            $cleaned_statistics[$query] = intval($count);
        }

        foreach ($cleaned_statistics as $query => $count) {
            // Check query is a string or number
            if (!is_string($query) && !is_numeric($query)) {
                Common::logger()->warning('Query is not a string or number: ' . json_encode($query),
                    array('function' => __FUNCTION__, 'file' => basename(__FILE__), 'line' => __LINE__));
                continue;
            }
            // Does the query contain a +?
            $searches = str_replace('+', ' ', strval($query));
            $searches = strtolower($searches);

            // see whether the array already contains an entry with query = $statistic['query']
            if ($query != $searches && array_key_exists($searches, $cleaned_statistics)) {
                // add the count to the existing entry
                $cleaned_statistics[$searches] += $count;
                // Remove $query from the array $cleaned_statistics
                unset($cleaned_statistics[$query]);
            } else {
                $cleaned_statistics[$searches] = intval($count);
            }
        }

        // Get list of terms to remove - contained in file terms_to_remove.txt in the Web root
        $terms_to_remove = file_get_contents(ABSPATH . '/search_terms_to_remove.txt');
        if ($terms_to_remove) {
            Common::logger()->info('Pruning Search Terms',
                array('function' => __FUNCTION__, 'file' => basename(__FILE__), 'line' => __LINE__));

            // Remove any terms in the list from the array
            $terms_to_remove = explode("\n", $terms_to_remove);
            foreach ($terms_to_remove as $term_to_remove) {
                if (array_key_exists($term_to_remove, $cleaned_statistics)) {
                    Common::logger()->info('Removing Search Term: ' . $term_to_remove,
                        array('function' => __FUNCTION__, 'file' => basename(__FILE__), 'line' => __LINE__));
                    unset($cleaned_statistics[$term_to_remove]);
                }
            }
        }

        // Finally update the option
        $popular_searches = array();
        foreach ($cleaned_statistics as $query => $count) {
            $popular_searches[] = array('query' => $query, 'count' => $count);
        }

        // Get the first 10 entries
        Common::logger()->info('Updating Popular Searches -> ' . json_encode(array_slice($popular_searches,
                0, 10)),
            array('function' => __FUNCTION__, 'file' => basename(__FILE__), 'line' => __LINE__));
        update_option('popular_searches', $popular_searches);
    } catch (Throwable $e) {
        error_log(__FUNCTION__ . "/" . $e->getMessage());
        $status = 1;
    }
    try {
        Common::scgb_finalise($status);
    } catch (Throwable $e) {
        error_log('Failed whilst finalising: ' . __FUNCTION__ . '/' . $e->getMessage());
    }
}
