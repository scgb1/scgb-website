<?php
declare(strict_types=1);

require_once __DIR__ . '/../SCGB/Utils/scgb_autoload.php';

use SCGB\Common;
use SCGB\SkiService;

/**
 * Fetch core data from SkiService
 *
 * Configuration options:
 * - the raw data from the external APIs can be written to external files.
 * - the data loaded into the database is written to CSVs to avoid having to access the database externally
 *
 * Writes to one table
 * - scgb_resort_data - data specific to each resort
 *
 * @throws Exception
 */

function scgb_get_ski_service_data() : void
{
    try {
        $status = 0;
        Common::scgb_initialise(__FUNCTION__);

        /**
         * Main processing
         */
        // Get the data from the Ski Service API
        $skiServiceCollector = new SkiService();
        $skiServiceData = $skiServiceCollector->getSkiServiceResortData();

        // Create a query of all public posts of type sc_resort with the meta_key resort_ski_service_id
        $args = array(
            'posts_per_page'   => -1,
            'post_type' => 'sc_resort',
            'orderby' => 'resort_name',
            'order' => 'ASC',
            'meta_query' => array(
                array(
                    'key' => 'resort_ski_service_id',
                    'value' => '',
                    'compare' => '!='
                )
            )
        );

        // Iterate through the query and update the database with any data from the Ski Service API
        $query = new WP_Query($args);
        wp_cache_flush();
        while ($query->have_posts())
        {
            $query->the_post();
            $post_id = get_the_ID();
            $resort_name = html_entity_decode(get_the_title());

            $resort_ski_service_id = get_post_meta($post_id, 'resort_ski_service_id', true);

            // if the resort_ski_service_id is blank then we can't do anything
            if ($resort_ski_service_id == '') {
                continue;
            }

            Common::logger()->info('Processing ' . $resort_name . ' with resort_ski_service_id ' . $resort_ski_service_id,
                array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,));

            // Get the data from the Ski Service API
            if (!key_exists($resort_ski_service_id, $skiServiceData)) {
                Common::logger()->error('No data for ' . $resort_name . ' with resort_ski_service_id ' . $resort_ski_service_id,
                    array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,));
                continue;
            }
            $skiServiceCollector->updateResortData($post_id, intval($resort_ski_service_id));
            // If WP-Rocket is installed then clear the cache for this post
            if (function_exists('rocket_clean_post')) {
                rocket_clean_post($post_id);
            }
        }

    } catch (Exception $e) {
        error_log(__FUNCTION__ . 'Failed whilst gathering data: ' . $e->getMessage());
        $status = 1;
    }

    // All done
    try {
        Common::scgb_finalise($status);
    } catch (Exception $e) {
        error_log(
            'Failed whilst finalising: ' . __FUNCTION__ ."/" . $e->getMessage());
    }
}
