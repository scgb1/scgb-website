<?php
declare(strict_types=1);

require_once __DIR__ . '/../SCGB/Utils/scgb_autoload.php';


use SCGB\Common;

/**
 * Simple job to prune the log directory and also keep the site debug.log file to a reasonable size
 */

function scgb_daily_keep_log_directory_clean() : void
{
    $status = 0;
    try {
        Common::scgb_initialise(__FUNCTION__);

        $scgb_log_dir = $_ENV['SCGB_LOG_DIR'];

        if (!file_exists($scgb_log_dir)) {
            Common::logger()->error('Log directory ' . $scgb_log_dir . ' does not exist',
                array('function' => __FUNCTION__, 'file' => basename(__FILE__),  'line' => __LINE__,));
            $status = 1;
            throw new Exception('Log directory ' . $scgb_log_dir . ' does not exist');
        }

        $debug_log_file = ABSPATH . 'wp-content/debug.log';

        $dated_debug_log_file = $scgb_log_dir . '/' . date('Y-m-d') . '-debug.log';

        // If the dated file already exists then skip the move
        if (!file_exists($dated_debug_log_file)) {
            if (file_exists($debug_log_file)) {
                Common::logger()->debug('Moving ' . $debug_log_file . ' to ' . $dated_debug_log_file,
                    array('function' => __FUNCTION__, 'file' => basename(__FILE__),  'line' => __LINE__,));
                rename($debug_log_file, $dated_debug_log_file);
            }
        }

        // Now delete any files older than 7 days
        $files = glob($scgb_log_dir . '/*');
        $now = time();
        foreach ($files as $file) {
            if (is_file($file)) {
                if ($now - filemtime($file) >= 60 * 60 * 24 * 7) { // 7 days
                    Common::logger()->debug('Deleting ' . $file,
                        array('function' => __FUNCTION__, 'file' => basename(__FILE__),  'line' => __LINE__,));
                    unlink($file);
                }
            }
        }
    } catch (Exception $e) {
        error_log(__FUNCTION__ . ': Failed whilst cleaning logs: ' . $e->getMessage());
        $status = 1;
    }

    // All done
    try {
        Common::scgb_finalise($status);
    } catch (Exception $e) {
        wp_die(
            'Failed whilst finalising: ' . __FUNCTION__ . "/" . $e->getMessage());
    }
}