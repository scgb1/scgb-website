<?php

declare(strict_types=1);

require_once __DIR__ . '/../SCGB/Utils/scgb_autoload.php';

use SCGB\Common;

/**
 * Update the discount sort order - give preference to preferred affiliates
 */
function scgb_daily_update_discount_sort_order() : void
{
    try {
        $status = 0;
        Common::scgb_initialise(__FUNCTION__);

        $arr_featured_partners = [];
        $arr_other_partners = [];

        $args = array(
            'post_type' => 'sc_discount',
            'posts_per_page' => -1,
        );
        $posts = get_posts($args);
        $post_lookup = array();
        foreach ($posts as $post) {
            $post_lookup[$post->ID] = $post;
            $featured_partner = get_post_meta($post->ID, 'discount_featured', true);
            if ($featured_partner == 'yes' || $featured_partner == "1") {
                $arr_featured_partners[] = $post->ID;
            } else {
                $arr_other_partners[] = $post->ID;
            }
        }

        // randomise the order of both arrays
        $count = 1;
        shuffle($arr_featured_partners);
        foreach ($arr_featured_partners as $post_id) {
            $post = $post_lookup[$post_id];
            Common::logger()->debug('Order: ' . $count++ . ', ID: ' . $post->ID . ', Name: ' . $post->post_title .
                ', Featured Partner: yes',
                array('function' => __FUNCTION__, 'file' => basename(__FILE__), 'line' => __LINE__));
        }
        shuffle($arr_other_partners);
        foreach ($arr_other_partners as $post_id) {
            $post = $post_lookup[$post_id];
            Common::logger()->debug('Order: ' . $count++ . ', ID: ' . $post->ID . ', Name: ' . $post->post_title .
                ', Featured Partner: no',
                array('function' => __FUNCTION__, 'file' => basename(__FILE__), 'line' => __LINE__));
        }

        // combine the two arrays with preferred affiliates first
        $discounts = array_merge($arr_featured_partners, $arr_other_partners);

        // update the sort order
        Common::update_post_menu_order($discounts);
    } catch (Exception $e) {
        error_log(__FUNCTION__ . "Exception: " . $e->getMessage());
        $status = 1;
    }

    try {
        Common::scgb_finalise($status);
    } catch (Exception $e) {
        error_log("Exception: " . __FUNCTION__ . "/" . $e->getMessage());
    }
}
