<?php
declare(strict_types=1);

require_once __DIR__ . '/../SCGB/Utils/scgb_autoload.php';

use SCGB\CollectorDTN;
use SCGB\Common;
use SCGB\WeatherData;

/**
 * Resort Weather Loader for the SkiClub of Great Britain website - https://skiclub.co.uk.
 *
 * Fetch current weather and 10-day forecast for each resort
 *
 * Updates two tables in the website database:
 * - scgb_resort_data - current weather and resort conditions - also updated by another function
 * - scgb_resort_forecast - 10-day weather forecast
 */

    function scgb_get_resort_weather() : void
{
    $status = 0;
    $num_forecasts = null;
    $forecast_count = 0;
    try {
        Common::scgb_initialise(__FUNCTION__);

        // Set up the collector - for now only use legacy DTN as a source
        $collector = new CollectorDTN();

        // Create a query of all public posts of type sc_resort which have a longitude set
        $args = array(
            'posts_per_page'   => -1,
            'post_type' => 'sc_resort',
            'post_status' => 'publish',
            'orderby' => 'title',
            'order' => 'ASC',
            'meta_query' => array(
                array(
                    'key' => 'resort_longitude',
                    'value' => '',
                    'compare' => '!='
                )
            )
        );

        // Iterate through the query and update the database with any data from the Ski Service API
        $query = new WP_Query($args);
        wp_cache_flush();
        $num_forecasts = $query->post_count;
        $forecast_count = 0;
        while ($query->have_posts())
        {
            $query->the_post();
            $post_id = get_the_ID();
            $title = html_entity_decode(get_the_title());

            Common::logger()->info('Processing ' . $title,
                array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,));

            $resort_longitude = get_post_meta($post_id, 'resort_longitude', true);
            $resort_latitude = get_post_meta($post_id, 'resort_latitude', true);
            $resort_highest_longitude = get_post_meta($post_id, 'resort_highest_longitude', true);
            $resort_highest_latitude = get_post_meta($post_id, 'resort_highest_latitude', true);

            try {
                $resort_weather = scgb_get_weather_for_location(
                    $title, $collector, $resort_latitude, $resort_longitude);
                $resort_highest_weather = scgb_get_weather_for_location(
                    $title, $collector, $resort_highest_latitude, $resort_highest_longitude);
            } catch (Exception $exception) {
                Common::logger()->error('Failed to get weather for ' . $title . ', ' .
                    $exception->getMessage(),
                    array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,));
                continue;
            }

            $resort_weather_data = new WeatherData($post_id, $resort_weather, $resort_highest_weather);
            if (!$resort_weather_data->is_valid()) {
                Common::logger()->error('Invalid weather data for ' . $title . ' with post_id ' .
                    $post_id,
                    array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,));
                $status = 1;
                continue;
            }
            scgb_check_api_id_set($post_id);
            $resort_weather_data->save_to_wordpress();
            $forecast_count++;

            // If WP-Rocket is installed then clear the cache for this post
            if (function_exists('rocket_clean_post')) {
                rocket_clean_post($post_id);
            }

            // Destroy the object to free up memory
            unset($resort_weather_data);
        }

    } catch (Exception $e) {
        error_log(__FUNCTION__ . ': Failed whilst gathering data: ' . $e->getMessage());
        $status = 1;
    }

    // All done
    try {
        if ($num_forecasts != $forecast_count) {
            $status = 1;
            Common::logger()->error('Only ' . $forecast_count . ' out of ' . $num_forecasts .
                ' successfully processed',
                array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,));
        }
        Common::scgb_finalise($status);
    } catch (Exception $e) {
        wp_die(
            'Failed whilst finalising: ' . __FUNCTION__ . "/" . $e->getMessage());
    }
}

/**
 * Helper function to check that the meta_key resort_weather_resort_api_id is set for the resort
 * It's still needed by the redbullet weather logic
 * It used to be independent but is now always the post_id
 * @throws Exception
 */
function scgb_check_api_id_set(int $post_id) : void
{
    $api_id = get_post_meta($post_id, 'resort_weather_resort_api_id', true);
    if (strval($post_id) != $api_id) {
        Common::logger()->error('Updating resort_weather_resort_api_id for ' . $post_id,
            array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,));

        update_post_meta($post_id, 'resort_weather_resort_api_id', strval($post_id));
    }
}

/**
 * Fetch the weather for a given set of co-ordinates
 *
 * Check everything makes sense before proceeding
 *
 * @param string $resort_name - used for logging
 * @param CollectorDTN $collector - where we get the data from
 * @param mixed $lat - should be a float but we need to check
 * @param mixed $long - should be a float but we need to check
 * @throws Exception - if nothing makes sense
 */
function scgb_get_weather_for_location(string $resort_name, CollectorDTN $collector,
                                       mixed  $lat, mixed $long) : ?array
{
    // Make sure lat and long both numbers
    if (!$lat || $lat == '') {
        Common::logger()->error("Invalid latitude for " . $resort_name . " - " . $lat,
            array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,));
        return null;
    }
    if (!$long || $long == '') {
        Common::logger()->error("Invalid longitude for " . $resort_name . " - " . $long,
            array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,));
        return null;
    }
    $lat = floatval($lat);
    $long = floatval($long);

    // Check that they are valid
    if ($lat < -90 || $lat > 90) {
        Common::logger()->warning("Invalid latitude for " . $resort_name . " - " . $lat,
            array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,));
        return null;
    }
    if ($long < -180 || $long > 180) {
        Common::logger()->warning("Invalid longitude for " . $resort_name . " - " . $long,
            array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,));
        return null;
    }

    return $collector->getForecastForLocation($lat, $long);
}
