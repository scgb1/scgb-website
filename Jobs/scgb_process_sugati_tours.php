<?php
declare(strict_types=1);

use SCGB\Common;
use SCGB\SugatiTour;

require_once __DIR__ . '/../SCGB/Utils/scgb_autoload.php';

/**
 * For each holiday check which tours (departures) are still available and then:
 * - ensure the most appropriate price is displayed
 * - set the holiday sort order - we focus on holidays with availability
 * - set 'Holidays You May Like' on the holidays page
 *
 * - If there are departed tours then make sure the Reviews Tab exists
 * - Set a button to book on the same tour (or other holidays) depending on availability
 *
 * @throws Exception
 */
function scgb_process_sugati_tours() : void
{
    try{
        Common::scgb_initialise(__FUNCTION__);

        $status = 0;
        $holidays_with_availability = array();
        $holiday_status_array = array();

        // load Holidays from Sugati
        SugatiTour::initialise();

        // Load the Holidays - which have a reference to Sugati
        $args = array(
            'posts_per_page'   => -1,
            'post_type' => 'holiday',
            'post_status' => 'publish',
            'meta_query' => array(
                array(
                    'key' => 'holiday_tour_id_in_sugati',
                    'value' => '',
                    'compare' => '!='
                )
            )
        );

        // Run through each holiday and reconcile with data from Sugati
        // If there is availability - what is the lowest price
        // if no availability but still departures - which is the earliest date
        $query = new WP_Query($args);
        wp_cache_flush();
        while ($query->have_posts())
        {
            $query->the_post();
            $post_id = get_the_ID();
            $holiday_name = html_entity_decode(get_the_title());
            Common::logger()->info("Processing holiday: " . $post_id . "/" . $holiday_name,
                array('function' => __FUNCTION__, 'file' => basename(__FILE__), 'line' => __LINE__));

            $holiday_status = scgb_check_for_updates_from_sugati($post_id, $holiday_name);
            if ($holiday_status !== null &&  $holiday_status['earliest_date_with_availability'] !== null) {
                $holidays_with_availability[] = $post_id;
            }
            $holiday_status_array[$post_id] = $holiday_status;
        }

        // Set the site wide display order - of particular interest on the holidays page - we want to focus
        // on holidays with availability and which are leaving soon
        scgb_update_holiday_order($holiday_status_array);

        // Random display of holidays with availability on the holidays page
        scgb_update_holidays_you_may_like($holidays_with_availability);
    }catch (Exception $e){
        error_log(__FUNCTION__ . 'Failed to update WPHoliday: ' . $e->getMessage());
        $status = 1;
    }
    try {
        Common::scgb_finalise($status);
    } catch (Exception $e) {
        error_log('Failed to finalise: ' . __FUNCTION__ . "/" . $e->getMessage());
    }
}

function scgb_update_holiday_order(array $holiday_status_array) : void
{
    // sort the array using a custom function - want holidays with availability first
    usort($holiday_status_array, 'scgb_holiday_compare');

    $menu_order_array = array();
    foreach ($holiday_status_array as $item) {
        $menu_order_array[] = $item['post_id'];
    }

    // now update the menu order
    Common::update_post_menu_order($menu_order_array);

}

/**
 * Holiday compare function:
 * - availability first
 * - sold out but still in the future
 * - the rest by holiday name
 * @param $a
 * @param $b
 * @return int
 */
function scgb_holiday_compare($a, $b) : int
{
    // First check is whether both have availability
    if ($a['earliest_date_with_availability'] !== null && $b['earliest_date_with_availability'] !== null) {
        // Both have availability - sort by earliest date
        if ($a['earliest_date_with_availability'] == $b['earliest_date_with_availability']) {
            return 0;
        }
        return ($a['earliest_date_with_availability'] < $b['earliest_date_with_availability']) ? -1 : 1;
    } elseif ($a['earliest_date_with_availability'] !== null && $b['earliest_date_with_availability'] === null) {
        // Only $a has availability
        return -1;
    } elseif ($a['earliest_date_with_availability'] === null && $b['earliest_date_with_availability'] !== null) {
        // Only $b has availability
        return 1;
    }

    // At this point we know that neither have availability so check the departure date
    if ($a['earliest_date_departure'] !== null && $b['earliest_date_departure'] !== null) {
        // Both have departure dates - sort by earliest date
        if ($a['earliest_date_departure'] == $b['earliest_date_departure']) {
            return 0;
        }
        return ($a['earliest_date_departure'] < $b['earliest_date_departure']) ? -1 : 1;
    } elseif ($a['earliest_date_departure'] !== null && $b['earliest_date_departure'] === null) {
        // Only $a has a departure date
        return -1;
    } elseif ($a['earliest_date_departure'] === null && $b['earliest_date_departure'] !== null) {
        // Only $b has a departure date
        return 1;
    }

    // Now we know that there is no availability and no departure date so sort by name
    return strcmp($a['holiday_name'], $b['holiday_name']);
}

/**
 * @param int $post_id - the holiday in question
 * @param string $holiday_name - for debug purposes only
 * @throws Exception
 */
function scgb_check_for_updates_from_sugati(int $post_id, string $holiday_name) : ?array
{
    $lowest_price = null;
    $departure_months_array = []; // The website needs to know which months have departures
    $earliest_date_with_availability = null;
    $earliest_departure_date = null;

    // Get the number of tours - if this is blank or not a number > 0 then skip
    $number_of_tours = get_post_meta($post_id, 'holiday_tour_id_in_sugati', true);
    if ($number_of_tours == null || !is_numeric($number_of_tours) || $number_of_tours <= 0) {
        Common::logger()->warning( "No tours found for '" . $holiday_name . "'" );
        return array(
            'post_id' => $post_id,
            'holiday_name' => $holiday_name,
            'earliest_date_with_availability' => null,
            'earliest_date_departure' => null,
        );
    }

    for ($count = 0; $count < $number_of_tours; $count++) {
        $tour_id = get_post_meta($post_id, 'holiday_tour_id_in_sugati_' . $count . '_id', true);
        $sugati_tour = SugatiTour::getSugatiTour($tour_id);
        if ($sugati_tour == null) {
            Common::logger()->warning( "Sugati Tour '" . $tour_id . "' not found for '" .
                $holiday_name . "'" );
            continue;
        }

        // If the start date is before today then skip
        if ($sugati_tour->getSugatiTourDepartureDate() < new DateTime()) {
            Common::logger()->info( 'Skipping ' . $sugati_tour->getSugatiTourName() .
                ' as it has already departed. Start Date -> ' .
                $sugati_tour->getSugatiTourDepartureDate()->format('Y-m-d'));
            continue;
        }

        if ($earliest_departure_date === null) {
            $earliest_departure_date = $sugati_tour->getSugatiTourDepartureDate();
        }else{
            if ($sugati_tour->getSugatiTourDepartureDate() < $earliest_departure_date) {
                $earliest_departure_date = $sugati_tour->getSugatiTourDepartureDate();
            }
        }

        if ($sugati_tour->getSugatiTourStatus() != 'On Sale' || $sugati_tour->getSugatiTourSpacesRemaining() == 0) {
            Common::logger()->info( 'Skipping ' . $sugati_tour->getSugatiTourName() .
                ' as it is not on sale. Status -> ' .
                $sugati_tour->getSugatiTourStatus() . ', Spaces Remaining -> ' .
                $sugati_tour->getSugatiTourSpacesRemaining());
            continue;
        }
        if ($earliest_date_with_availability === null) {
            $earliest_date_with_availability = $sugati_tour->getSugatiTourDepartureDate();
        }else {
            if ($sugati_tour->getSugatiTourDepartureDate() < $earliest_date_with_availability) {
                $earliest_date_with_availability = $sugati_tour->getSugatiTourDepartureDate();
            }
        }

        if ($lowest_price === null  || $sugati_tour->getSugatiTourPrice() < $lowest_price) {
            $lowest_price = $sugati_tour->getSugatiTourPrice();
        }
        $departureMonth = $sugati_tour->getSugatiTourDepartureDate()->format('F');
        if (!in_array($departureMonth, $departure_months_array)) {
            $departure_months_array[] = $departureMonth;
        }
    }

    if ($lowest_price !== null) {
        $holidayPrice = 'From £' . $lowest_price;
    } elseif ($earliest_departure_date !== null) {
        $holidayPrice = 'No Holidays Available';
    } else {
        $holidayPrice = 'Departed';
    }
    $departure_months_array = array_map('ucfirst', $departure_months_array);

    scgb_check_departure_months($post_id, $holiday_name, $departure_months_array);

    // See if the website needs updating
    $current_price = get_post_meta($post_id, 'holiday_price_note', true);
    if ($current_price != $holidayPrice) {
        update_post_meta($post_id, 'holiday_price_note', $holidayPrice);
        // If WP-Rocket is installed then clear the cache for this post
        if (function_exists('rocket_clean_post')) {
            rocket_clean_post($post_id);
        }
    }

    return (array(
        'post_id' => $post_id,
        'holiday_name' => $holiday_name,
        'earliest_date_with_availability' => $earliest_date_with_availability,
        'earliest_date_departure' => $earliest_departure_date,
    ));
}

/**
 * @throws Exception
 */
function scgb_check_departure_months($post_id, $holiday_name, $arrDepartureMonths) : void
{
    $current_departure_months = get_post_meta($post_id, 'holiday_departure_months', true);
    if ($current_departure_months == null) {
        $current_departure_months = [];
    }
    if ($current_departure_months == $arrDepartureMonths)
        return;

    update_post_meta($post_id, 'holiday_departure_months', $arrDepartureMonths);

    Common::logger()->info( "Holiday: " . $holiday_name .
        " - Updated Departure Months from " . serialize($current_departure_months) . " to " .
        serialize($arrDepartureMonths));
}

/**
 * @throws Exception
 */
function scgb_update_holidays_you_may_like(array $holidays_with_availability) : bool
{
    // Holidays you may like - pick 4 random ones from the list of Holidays which still have availability
    // Bit of a hack as the holidays post is hard coded at this point
    $holidays_post_id = 4030;
    $you_like_meta_key = 'sections_2_component_you_may_also_like_posts';
    $you_like_section_meta_key = 'sections_2_section_display_status';
    $you_like_heading_meta_key = 'sections_2_heading';
    $you_like_heading = 'Holidays you might like';

    $you_like_check = get_post_meta($holidays_post_id, $you_like_heading_meta_key, true);
    if ($you_like_check != $you_like_heading) {
        Common::logger()->error( "Holidays you may like section seems to have changed. Expected '" .
            $you_like_heading . "' but found '" . $you_like_check . "'");
        return false;
    }
    $holidays_you_make_like_visibility = get_post_meta($holidays_post_id, $you_like_section_meta_key, true);
    if (count($holidays_with_availability) == 0 && $holidays_you_make_like_visibility != 'hide') {
        update_post_meta($holidays_post_id, $you_like_section_meta_key, 'hide');
        Common::logger()->info( "Holidays you may like section has been hidden as there " .
                "are no holidays with availability");

        return true;
    }

    if (count($holidays_with_availability) > 4) {
        $arrHolidaysYouMayLike = array();
        for ($i = 0; $i < min(4, count($holidays_with_availability)); $i++) {
            $rand = rand(0, count($holidays_with_availability) - 1);
            $likeHoliday = array_slice($holidays_with_availability, $rand, 1);
            // Get first element of array
            $likeHoliday = array_shift($likeHoliday);
            $arrHolidaysYouMayLike[] = $likeHoliday;
            unset($holidays_with_availability[$likeHoliday]);
        }
        $holidays_with_availability = array_values($arrHolidaysYouMayLike);
    } else {
        // randomise the array
        shuffle($holidays_with_availability);
    }

    update_post_meta($holidays_post_id, $you_like_meta_key, $holidays_with_availability);

    if ($holidays_you_make_like_visibility != 'logged') {
        update_post_meta($holidays_post_id, $you_like_section_meta_key, 'logged');
        Common::logger()->info( "Holidays you may like section has been shown as there " .
            "are holidays with availability");
    }
    return true;
}

