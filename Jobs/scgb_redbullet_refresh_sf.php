<?php

use SCGB\Common;

require_once __DIR__ . '/../SCGB/Utils/scgb_autoload.php';

/**
 * Hourly function for redbullet
 *
 * Gets Sugati information
 * Keeps Salesforce Tokens fresh!
 * We have a wee hack in there for running in debug
 *
 * @throws Exception
 */
function scgb_redbullet_refresh_sf() : void
{
    $status = 0;
    try{
        Common::scgb_initialise(__FUNCTION__);

        $debugInclude = '../Wordpress/skiclub/wp-content/themes/skiclub/functions.php';
        if (file_exists($debugInclude)) {
            require_once $debugInclude;
        }
        wp_cache_flush();
        get_sugati_holidays_to_json();
        sf_get_token();
        session_write_close();

    } catch (Exception $e){
        error_log(__FUNCTION__ . 'Failed to update WPHoliday: ' . $e->getMessage());
        $status = 1;
    }
    try {
        Common::scgb_finalise($status);
    } catch (Exception $e) {
        error_log('Failed to finalise: ' . __FUNCTION__ . "/" . $e->getMessage());
    }
}
