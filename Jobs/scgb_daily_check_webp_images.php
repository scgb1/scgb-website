<?php
declare(strict_types=1);

require_once __DIR__ . '/../SCGB/Utils/scgb_autoload.php';

use SCGB\Common;

/**
 * The site uses a plugin called Smush to generate WebP images, but it doesn't pick up all the images used by the site,
 * so we need to check for any images that not WebP ad generate them if required.
 */

function scgb_daily_check_webp_images() : void
{
    $status = 0;
    try {
        Common::scgb_initialise(__FUNCTION__);

        // Iterate through files in the uploads directory and optimise them
        $upload_dir = wp_get_upload_dir();
        $upload_dir = $upload_dir['basedir'];

        $directory = new RecursiveDirectoryIterator($upload_dir);
        $iterator = new RecursiveIteratorIterator($directory);
        foreach ($iterator as $info) {
            $image_file = $info->getPathname();
            // if a directory or not an image, skip it
            if ($info->isDir() || !in_array($info->getExtension(), array('jpg', 'jpeg', 'png'))) {
                continue;
            }

            if (!preg_match('#wp-content/uploads/202[0-9]#', $image_file)) {
                continue;
            }

            Common::logger()->debug('Checking image ' . $image_file);

            if (preg_match('/\.jpg$/', $image_file) || preg_match('/\.jpeg$/', $image_file) ||
                preg_match('/\.png$/', $image_file)) {
                $smush_image_file = preg_replace('/\.jpg$/', '.jpg.webp', $image_file);
                $smush_image_file = preg_replace('/\.jpeg$/', '.jpeg.webp', $smush_image_file);
                $smush_image_file = preg_replace('/\.png$/', '.png.webp', $smush_image_file);
                $smush_image_file = str_replace('/uploads/', '/smush-webp/', $smush_image_file);

                $needs_conversion = true;

                // Does the webp file exist?
                if (file_exists($smush_image_file)) {
                    // is the webp file older than the original?
                    if (filemtime($smush_image_file) > filemtime($image_file)) {
                        $needs_conversion = false;
                    }
                }
                if ($needs_conversion) {
                    {
                        // Create the smushed image
                        // create a GDI image from the original
                        $image_type = exif_imagetype($image_file);
                        $orig_image = null;
                        $orig_image = match ($image_type) {
                            IMAGETYPE_JPEG => imagecreatefromjpeg($image_file),
                            IMAGETYPE_PNG => imagecreatefrompng($image_file),
                            IMAGETYPE_GIF => imagecreatefromgif($image_file),
                            default => null,
                        };
                        if ($orig_image === null) {
                            continue;
                        }

                        imagepalettetotruecolor($orig_image);
                        imagealphablending($orig_image, true);
                        imagesavealpha($orig_image, true);

                        // Make sure the target directory exists
                        $target_dir = dirname($smush_image_file);
                        if (!file_exists($target_dir)) {
                            mkdir($target_dir, 0750, true);
                        }
                        $result = imagewebp($orig_image, $smush_image_file);
                        if (!$result) {
                            Common::logger()->error('webp image creation succeeded for ' . $image_file . ' -> ' .
                                $smush_image_file,
                                array('function' => __FUNCTION__, 'file' => basename(__FILE__),  'line' => __LINE__,));
                        } else {
                            Common::logger()->info('webp image creation succeeded for ' . $image_file . ' -> ' .
                                $smush_image_file,
                                array('function' => __FUNCTION__, 'file' => basename(__FILE__),  'line' => __LINE__,));
                        }
                        imagedestroy($orig_image);
                    }
                }
            }
        }
    } catch (Exception $e) {
        error_log(__FUNCTION__ . ': Failed whilst gathering data: ' . $e->getMessage());
        $status = 1;
    }

    // All done
    try {
        Common::scgb_finalise($status);
    } catch (Exception $e) {
        wp_die(
            'Failed whilst finalising: ' . __FUNCTION__ . "/" . $e->getMessage());
    }
}