<?php
declare(strict_types=1);

require_once __DIR__ . '/../SCGB/Utils/scgb_autoload.php';

use SCGB\Common;

/**
 * Check that the Resort Elevations in the Weather Tab match the attributes set for the Resort
 *
 */

function scgb_daily_check_resort_elevations() : void
{
    $status = 0;
    try {
        Common::scgb_initialise(__FUNCTION__);

         // Create a query of all public posts of type sc_resort are published
        $args = array(
            'posts_per_page'   => -1,
            'post_type' => 'sc_resort',
            'post_status' => 'publish',
            'orderby' => 'title',
            'order' => 'ASC',
        );

        // Iterate through the query and run the checks
        $query = new WP_Query($args);
        wp_cache_flush();
        while ($query->have_posts())
        {
            $query->the_post();
            $post_id = get_the_ID();
            $title = html_entity_decode(get_the_title());

            Common::logger()->info('Processing ' . $title,
                array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,));

            $changed = 0;
            $resort_height = get_post_meta($post_id, 'resort_height', true);
            $resort_highest_level = get_post_meta($post_id, 'resort_highest_level', true);

            // Now need to find the tab which has the weather
            $weather_tab = scgb_find_tab($post_id, 'Weather');
            if ($weather_tab == -1) {
                continue;
            }

            $meta_key = 'resort_tabs_' . $weather_tab .  '_sections';
            $weather_components = get_post_meta($post_id, $meta_key, true);

            foreach ($weather_components as $index => $weather_component) {
                if ($weather_component == 'component_resort_weather') {
                    $meta_key = 'resort_tabs_' . $weather_tab . '_sections_' . $index .
                        '_component_resort_weather_layout';
                    $component_type = get_post_meta($post_id, $meta_key, true);
                    if ($component_type == '1') {
                        $changed += scgb_check_elevation_string($post_id, $weather_tab, $index, true,
                            $resort_highest_level);
                    } elseif ($component_type == '2') {
                        $changed += scgb_check_elevation_string($post_id, $weather_tab, $index, false,
                            $resort_height);
                    }
                }
            }

            // If WP-Rocket is installed then clear the cache for this post if we have updated it
            if ($changed && function_exists('rocket_clean_post')) {
                rocket_clean_post($post_id);
            }
        }

    } catch (Exception $e) {
        error_log(__FUNCTION__ . ': Failed whilst gathering data: ' . $e->getMessage());
        $status = 1;
    }

    // All done
    try {
        Common::scgb_finalise($status);
    } catch (Exception $e) {
        wp_die(
            'Failed whilst finalising: ' . __FUNCTION__ . "/" . $e->getMessage());
    }
}

/**
 * @throws Exception
 */
function scgb_find_tab(int $post_id, string $target_tab_name) : int
{
    $num_tabs = get_post_meta($post_id, 'resort_tabs', true);
    $found = false;
    for ($tab_number = 0; $tab_number < $num_tabs; $tab_number++)
    {
        $meta_key = 'resort_tabs_' . $tab_number .  '_tab_name';
        $tab_name = get_post_meta($post_id, $meta_key, true);
        if ($tab_name == $target_tab_name) {
            $found = true;
            break;
        }
    }
    if (!$found) {
        $tab_number = -1;
        Common::logger()->error('No ' . $target_tab_name . ' tab found for post ' . $post_id,
            array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,));
    }
    return $tab_number;
}

/**
 * @throws Exception
 */
function scgb_check_elevation_string(int $post_id, int $weather_tab, int $section, bool $is_top, string $height ) : int
{
    $changed = 0;
    $elevation_string = $is_top ? 'Top' : 'Resort';

    if ($height != '')
        $elevation_string = $elevation_string . ' (' . $height . 'm)';

    $meta_key = 'resort_tabs_' . $weather_tab . '_sections_' . $section . '_heading';
    $current_string = get_post_meta($post_id, $meta_key, true);
    if ($current_string != $elevation_string) {
        Common::logger()->info('Updating elevation heading from ' . $current_string .
            ' to ' . $elevation_string,
            array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,));
        update_post_meta($post_id, $meta_key, $elevation_string);
        $changed = 1;
    }

    return $changed;
}