<?php

declare(strict_types=1);

require_once __DIR__ . '/../SCGB/Utils/scgb_autoload.php';

use SCGB\Common;

/**
 * Delete expired repping slots
 * Iterate through all posts of type 'sc_rep'
 * Look for a meta_key of 'rep_slots' which has a value > 0
 * Look in the sub keys of form 'rep_slots_' . count . '_repping_date
 * Delete any which have expired
 */
function scgb_delete_expired_repping_slots() : void
{
    try {
        Common::scgb_initialise(__FUNCTION__);
        $args = array(
            'post_type' => 'sc_rep',
            'posts_per_page' => -1,
        );

        $posts = get_posts($args);
        foreach ($posts as $post) {
            wp_cache_flush();
            $rep_slots_meta = get_post_meta($post->ID);

            Common::logger()->debug('Post ID: ' . $post->ID . ', Rep Name: ' . $post->post_title,
                array('function' => __FUNCTION__, 'file' => basename(__FILE__), 'line' => __LINE__));
            // One off to clean to some bad data caused by a previous release
            $rep_slots = intval($rep_slots_meta['rep_slots'][0]);
            if (scgb_check_for_bad_rep_data($post->ID, $rep_slots_meta, $rep_slots)) {
                $rep_slots_meta = get_post_meta($post->ID);
            }
            if ($rep_slots > 0) {
                Common::logger()->debug('Rep Slots: ' . $rep_slots,
                    array('function' => __FUNCTION__, 'file' => basename(__FILE__), 'line' => __LINE__));

                for ($i = 0; $i < $rep_slots; $i++) {
                    $repping_slot = $rep_slots_meta['rep_slots_' . $i . '_repping_date'][0];
                    $rep_name = get_post_field('post_title', $post->ID);
                    if (scgb_rep_slot_expired($rep_name, $repping_slot)) {
                        Common::logger()->info("Deleting expired repping slot: " . $rep_name . '/' . $repping_slot,
                            array('function' => __FUNCTION__, 'file' => basename(__FILE__), 'line' => __LINE__));
                        scgb_copy_rep_slot_to_archive($post->ID, $rep_slots_meta, $i);
                        scgb_delete_rep_slot($post->ID, $rep_slots_meta, $i, $rep_slots);
                        $rep_slots--;
                    }
                }
            }
        }
        Common::scgb_finalise();
    } catch (Exception $e) {
        error_log("Exception: " . __FUNCTION__ . "/" . $e->getMessage());
    }
}

function scgb_delete_rep_slot(int $ID, array $post_metadata, int $slot_to_delete, $num_slots_now) : void
{
    foreach ($post_metadata as $meta_key => $meta_value) {
        // See whether this is a key we have to delete
        if (str_starts_with($meta_key, 'rep_slots_' . $slot_to_delete)  ||
            str_starts_with($meta_key, '_rep_slots_' . $slot_to_delete)) {
            // Delete the key
            delete_post_meta($ID, $meta_key);
        }
    }

    // Update number of slots
    update_post_meta($ID, 'rep_slots', $num_slots_now - 1);

    // See whether other ones we need to bring forward
    if ($slot_to_delete == $num_slots_now - 1) {
        // No more slots to bring forward
        return;
    }

    for ($i = $slot_to_delete + 1; $i < $num_slots_now; $i++) {
        foreach ($post_metadata as $meta_key => $meta_value) {
            // See whether this is a key we have to bring forward
            if (str_starts_with($meta_key, 'rep_slots_' . $i)  ||
                str_starts_with($meta_key, '_rep_slots_' . $i)) {
                // Bring the key forward
                $new_key = str_replace('rep_slots_' . $i, 'rep_slots_' . ($i - 1), $meta_key);
                delete_post_meta($ID, $meta_key);
                update_post_meta($ID, $new_key, $meta_value[0]);
            }
        }
    }
}

/**
 * @throws Exception
 */
function scgb_copy_rep_slot_to_archive(int $ID, array $post_metadata, int $slot_to_delete) : void
{
    $conn = Common::getDBConnection();

    // Get the current number slots in the archive table for this Rep
    $sql = "select meta_value from scgb_postmeta where post_id = " . $ID . " and meta_key = 'repping_slots';";
    $result = $conn->query($sql);

    // If no slots in the archive database then set number slots to 0
    if ($result->num_rows == 0) {
        $num_slots = 0;
    } else {
        $row = $result->fetch_row();
        $num_slots = $row[0];
    }

    // Iterate through the data associated with the slot to be deleted and copy it to the next available position in
    // the archive table
    foreach ($post_metadata as $meta_key => $meta_value) {
        // See whether this is a key we have to copy
        if (str_starts_with($meta_key, 'rep_slots_' . $slot_to_delete)) {
            // Copy the key
            $new_key = str_replace('rep_slots_' . $slot_to_delete, 'rep_slots_' . $num_slots, $meta_key);
            $sql = "insert into scgb_postmeta (post_id, meta_key, meta_value) values (" .
                $ID . ", '" . $new_key . "', '" . $meta_value[0] . "');";
            $result = $conn->query($sql);
            if ($result === false) {
                throw new Exception('Failed to insert into scgb_postmeta: ' . $conn->error);
            }
        }
    }
    // Update number slots in the archive table - if 0 then this will be an insert
    if ($num_slots == 0) {
        $sql = "insert into scgb_postmeta (post_id, meta_key, meta_value) values (" .
            $ID . ", 'repping_slots', '1');";
    } else {
        $sql = "update scgb_postmeta set meta_value = " . ($num_slots + 1) .
            " where post_id = " . $ID . " and meta_key = 'repping_slots';";
    }
    $result = $conn->query($sql);
    if ($result === false) {
        throw new Exception('Failed to update scgb_postmeta: ' . $conn->error);
    }
}

/**
 * @throws Exception
 */
function scgb_check_for_bad_rep_data(int $ID, array $rep_slots_meta, int $num_slots) : bool
{
    // Looking for any metadata of the form rep_slots_{num} where {num} is greater than number slots - 1
    $bad_data = false;
    $rep_slots_bad_data = array();
    $key_num = null;
    foreach ($rep_slots_meta as $meta_key => $meta_value) {
        if (str_starts_with($meta_key, 'rep_slots_') || str_starts_with($meta_key, '_rep_slots_')) {
            // Extract the first number from the key
            $key_arr = explode('_', $meta_key);
            if ($key_arr[0] == 'rep') {
                $key_num = intval($key_arr[2]);
            } else {
                $key_num = intval($key_arr[3]);
            }

            if ($key_num >= $num_slots) {
                // This is bad data - copy it to the archive table and delete it
                $rep_slots_bad_data[$meta_key] = $meta_value;
                delete_post_meta($ID, $meta_key);
                Common::logger()->warning('Deleted bad data: ' . $meta_key . ' from post: ' . $ID,
                    array('function' => __FUNCTION__, 'file' => basename(__FILE__), 'line' => __LINE__));
                $bad_data = true;
            }
        }
    }
    if ($bad_data)
        scgb_copy_rep_slot_to_archive($ID, $rep_slots_bad_data, $key_num);
    return $bad_data;
}

/**
 * Determine whether a repping slot has expired
 * $repping slot is a string of the form DD/MM/YYYY [text] DD/MM/YYYY
 * @param string $rep_name
 * @param string $repping_slot
 * @return bool
 */
function scgb_rep_slot_expired(string $rep_name, string $repping_slot) : bool
{
    setlocale(LC_TIME, 'en_GB.utf8');

    // Split the string into start and end dates
    $repping_slot_arr = explode(' ', $repping_slot);
    // Make sure the date string is in the correct format
    // Assume the start and end dates in the same format and that there is only one separator
    $num_elements = count($repping_slot_arr);
    if ($num_elements != 3) {
        Common::logger()->warning('Badly formatted date in repping slot: ' . $rep_name . '/' . $repping_slot,
            array('function' => __FUNCTION__, 'file' => basename(__FILE__), 'line' => __LINE__));
        return false;
    }

    $repping_slot_end = $repping_slot_arr[2];

    $formatter = new IntlDateFormatter("it_IT", IntlDateFormatter::SHORT, IntlDateFormatter::NONE);
    $repping_slot_end_date=$formatter->parse($repping_slot_end);

    $datetime=new DateTime();
    $datetime->setTimestamp($repping_slot_end_date);

    // If the end date is in the past by more than 1 day then the slot has expired
    Common::logger()->debug("rep_name -> " . $rep_name . ", repping_slot -> " . $repping_slot .
        ", repping_slot_end -> " . $repping_slot_end .
        ", repping_slot_end_date -> " . $datetime->format('d/m/Y'),
        array('function' => __FUNCTION__, 'file' => basename(__FILE__), 'line' => __LINE__));
    if ($repping_slot_end_date < (time() - 86400)) {
        return true;
    }
    return false;
}
