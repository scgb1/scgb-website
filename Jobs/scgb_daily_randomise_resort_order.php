<?php
declare(strict_types=1);

require_once __DIR__ . '/../SCGB/Utils/scgb_autoload.php';

use SCGB\Common;

const arrWeights = array (
    'Freshtracks' => 8,
    'Rep' => 4,
    'Europe' => 5
);

/**
 * Set the menu_order for resorts - this drives the order in which they are displayed
 * both for weather and general search
 *
 * @throws Exception
 */
function scgb_daily_randomise_resort_order() : void
{
    $status = 0;
    try {
        Common::scgb_initialise(__FUNCTION__);

        $resorts_with_available_holidays = get_resorts_with_available_holidays();

        // We need resorts with co-ordinates (which should be everything) to calculate the distance between them
        $args = array(
            'posts_per_page'   => -1,
            'post_type' => 'sc_resort',
            'post_status' => 'publish',
            'meta_query' => array(
                array(
                    'key' => 'resort_longitude',
                    'value' => '',
                    'compare' => '!='
                )
            )
        );

        $resort_array = array();

        // Iterate through the query and update the database with any data from the Ski Service API
        $query = new WP_Query($args);

        while ($query->have_posts()) {
            $query->the_post();
            $post_id = get_the_ID();
            $resort_name = html_entity_decode(get_the_title());

            $resort_longitude = get_post_meta($post_id, 'resort_longitude', true);
            $resort_latitude = get_post_meta($post_id, 'resort_latitude', true);
            $resort_highest_longitude = get_post_meta($post_id, 'resort_highest_longitude', true);
            $resort_highest_latitude = get_post_meta($post_id, 'resort_highest_latitude', true);

            // If any of these are blank then skip this resort
            if (empty($resort_longitude) || empty($resort_latitude) || empty($resort_highest_longitude) ||
                empty($resort_highest_latitude)) {
                Common::logger()->error("resort $resort_name has incomplete co-ordinates");
                continue;
            }

            // Add a weight to the resort - to help with the randomisation
            // Need to determine if in Europe - they get extra weight in the algorithm
            $resort_array[$post_id]['weight'] = 1;
            $countries = get_the_terms($post_id, 'country');
            foreach($countries as $country) {
                if ($country->slug == 'europe') {
                    $resort_array[$post_id]['weight'] += arrWeights['Europe'];
                }
            }

            // See whether it's a Freshtracks resort (with availability) or a rep resort
            $resort_freshtrack_holidays = get_post_meta($post_id, 'resort_freshtrack_holidays', true);

            if ($resort_freshtrack_holidays == 'yes' && in_array($post_id, $resorts_with_available_holidays)) {
                $resort_array[$post_id]['weight'] += arrWeights['Freshtracks'];
            }
            $resort_rep_resort = get_post_meta($post_id, 'resort_rep_resort', true);
            if ($resort_rep_resort == 'yes') {
                $resort_array[$post_id]['weight'] += arrWeights['Rep'];
            }

            $resort_array[$post_id]['resort_name'] = $resort_name;
            $resort_array[$post_id]['post_id'] = $post_id;
            $resort_array[$post_id]['resort_longitude'] = $resort_longitude;
            $resort_array[$post_id]['resort_latitude'] = $resort_latitude;
            $resort_array[$post_id]['resort_highest_longitude'] = $resort_highest_longitude;
            $resort_array[$post_id]['resort_highest_latitude'] = $resort_highest_latitude;
        }

        scgb_update_resort_order($resort_array);
        scgb_update_resorts_you_may_like($resort_array);
    } catch (Exception $e) {
        error_log(__FUNCTION__ . 'Failed to randomise Resort: ' . $e->getMessage());
        $status = 1;
    }

    try {
        // Clean up and exit
        Common::scgb_finalise($status);
    } catch (Exception $e) {
        error_log('Failed to finalise: ' . __FUNCTION__ . "/" . $e->getMessage());
    }
}

function get_resorts_with_available_holidays() : array
{
    $resorts_with_available_holidays = array();

    $args = array(
        'posts_per_page'   => -1,
        'post_type' => 'holiday',
        'post_status' => 'publish',
    );

    // Iterate through the query and update the database with any data from the Ski Service API
    $query = new WP_Query($args);

    while ($query->have_posts()) {
        $query->the_post();
        $post_id = get_the_ID();
        $resorts = get_post_meta($post_id, 'holiday_resorts', true);
        $holiday_price_note = get_post_meta($post_id, 'holiday_price_note', true);

        if (!str_contains($holiday_price_note, '£')) {
            continue;
        }
        foreach ($resorts as $resort) {
            $resort = intval($resort);
            if (!in_array($resort, $resorts_with_available_holidays)) {
                $resorts_with_available_holidays[] = $resort;
            }
        }
    }
    return $resorts_with_available_holidays;
}


/**
 * Randomise the order of the array based on the 'weight' of the resort - the higher the weight the more
 * likely it is should be to be at the top of the list
 *
 * Algorithm as follows:
 * 1. Create an array of the weights (a positive integer) - with an array of resorts having the same weight
 * 2. Sum the weights
 * 3. Generate a random number between 0 and the sum of the weights
 * 4. Get a random entry from that array
 * 5. Remove that entry from the array
 * 6. Repeat steps 3-5 until the array is empty
 * 7. Recompute the sum of the weights if no more left at that weight
 *
 * @param array $resorts_array
 * @param int $num_required
 * @return array
 */
function randomise_resort_array(array $resorts_array, int $num_required = 1000000): array
{
    $weights_array = array();
    $randomised_resorts_array = array();
    foreach ($resorts_array as $item) {
        $weight = $item['weight'];
        if (!key_exists($weight, $weights_array)) {
            $weights_array[$weight]['weights'] = array();
        }
        $weights_array[$weight]['weights'][] = $item;
    }

    // Calculate a running weight total - current order of the array doesn't matter
    $weight_sum = 0;
    foreach ($weights_array as $weight => $item) {
        $weight_sum += $weight;
        $weights_array[$weight]['weight_total'] = $weight_sum;
    }

    while (count($resorts_array) > 0)
    {
        // generate a random number between 0 and the sum of the weights
        $random_number = rand(0, $weight_sum);

        // iterate through the array of weights and find the first one that is greater than the random number
        foreach ($weights_array as $weight => $items) {
            if ($random_number < $items['weight_total']) {
                // get a random item from the array - need to account for the 'weight_total' entry
                $random_number = rand(0, count($items['weights']) - 1);
                $random_Item = $items['weights'][$random_number];
                if (empty($random_Item)) {
                    continue;
                }
                $randomised_resorts_array[] = $random_Item;
                if (count($randomised_resorts_array) >= $num_required) {
                    break 2;
                }
                // remove the item from the array
                $key = $random_Item['post_id'];
                unset($resorts_array[$key]);

                // remove the item from the array of weights
                unset($weights_array[$weight]['weights'][$random_number]);
                $weights_array[$weight]['weights'] = array_values($weights_array[$weight]['weights']);

                // if the array of weights is now empty then remove it
                if (count($weights_array[$weight]['weights']) == 0) {
                    unset($weights_array[$weight]);
                    // recompute the sum of the weights
                    $weight_sum = 0;
                    foreach ($weights_array as $weight2 => $items2) {
                        $weight_sum += $weight2;
                        $weights_array[$weight2]['weight_total'] = $weight_sum;
                    }
                }
                break;
            }
        }
    }

    return $randomised_resorts_array;
}

/**
 * @throws Exception
 */
function scgb_update_resort_order($resort_array): void
{
    $randomised_resorts = randomise_resort_array($resort_array);
    $count = 1;

    global $wpdb;

    foreach ($randomised_resorts as $resort) {
        Common::logger()->debug($count . ' - Resort: ' . $resort['resort_name'],
            array('file' => basename(__FILE__), 'line' => __LINE__));
        $sql = "UPDATE wp_posts SET menu_order = " . $count . " WHERE ID = " . $resort['post_id'];
        $wpdb->query($sql);
        $count++;
    }
}

/**
 * @throws Exception
 */
function scgb_update_resorts_you_may_like($resort_array) : void
{
    // Calculate the distance between each resort and every other resort
    $resort_distances = array();
    foreach ($resort_array as $resort_id => $resort) {
        $resort_distances[$resort_id] = array();
        foreach ($resort_array as $resort_id2 => $resort2) {
            if ($resort == $resort2)
                continue;
            $resort_distances[$resort_id][$resort_id2] = scgb_calculate_distance(
                floatval($resort['resort_latitude']), floatval($resort['resort_longitude']),
                floatval($resort2['resort_latitude']), floatval($resort2['resort_longitude']));
        }
        // Sort the distances for this resort
        asort($resort_distances[$resort_id]);
    }

    $args = array(
        'posts_per_page'   => -1,
        'post_type' => 'sc_resort',
        'post_status' => 'publish',
        'orderby' => 'title',
        'order' => 'ASC',
        'meta_query' => array(
            array(
                'key' => 'sections_0_heading',
                'value' => 'Resorts You may also like',
                'compare' => '=='
            )
        )
    );

    // Iterate through the query and update the database with any data from the Ski Service API
    // Hack at this point as it requires the meta_key to be 'sections_0_component_you_may_also_like_posts'
    $query = new WP_Query($args);
    wp_cache_flush();
    while ($query->have_posts()) {
        $query->the_post();
        $post_id = $query->post->ID;
        $post_title = html_entity_decode(get_the_title($post_id));

        // Get 4 random resorts from the closest 50
        if (!key_exists($post_id, $resort_distances))
            continue;
        $closest_resorts = array_slice($resort_distances[$post_id], 0, 50, true);
        $resorts_you_may_like = array();
        foreach ($closest_resorts as $resort_id => $distance) {
            $resorts_you_may_like[$resort_id] = $resort_array[$resort_id];
        }

        // This post already removed from the list of resorts to like
        $resorts_you_may_like = randomise_resort_array($resorts_you_may_like, 4);
        $resorts_to_like = array();
        $count = 0;
        foreach ($resorts_you_may_like as $resort) {
            $resorts_to_like[$count++] = strval($resort['post_id']);
        }

        Common::logger()->debug('Resort you may like: ' . $post_title . ' - ' . implode(', ', $resorts_to_like),
            array('file' => basename(__FILE__), 'line' => __LINE__));

        // now update the post meta
        update_post_meta($post_id, 'sections_0_component_you_may_also_like_posts', $resorts_to_like);
    }
}
/**
 * Calculate the great circle distance (in km) between two points on the earth (specified in decimal degrees)
 * @throws Exception
 */
function scgb_calculate_distance(float $lat1, float $lon1, float $lat2, float $lon2) : float
{
    $theta = $lon1 - $lon2;
    $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2))
        + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
    $dist = acos($dist);
    $dist = rad2deg($dist);
    return $dist * 60 * 1.1515 * 1.609344;
}



