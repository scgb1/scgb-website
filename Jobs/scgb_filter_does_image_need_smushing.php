<?php

/**
 * Image optimisation filter. The site is using a plugin called Smush to generate webp images where it can but there
 * are some which it is not picking up. This filter runs with a priority of 1000 (so hopefully last) and looks
 * for jpg image urls going out the door which are not webp.   If it can't find the appropriate image in the smush
 * directory - then it creates it.
 *
 * @param $image
 * @return array|GdImage|resource
 */

function scgb_filter_does_image_need_smushing($image)
{
    global $post;
    if ($post !== null && is_array($image)) {
        // If the image ends in jpg or png and the browser accepts webp then try and change the image to webp
        $browser_accepts_webp = false;
        if (isset($_SERVER['HTTP_ACCEPT'])) {
            $browser_accepts_webp = str_contains($_SERVER['HTTP_ACCEPT'], 'image/webp');
        }
        if ($browser_accepts_webp &&
            (preg_match('/\.jpg$/', $image[0]) || preg_match('/\.png$/', $image[0]))) {
            $smush_image_url = preg_replace('/\.jpg$/', '.jpg.webp', $image[0]);
            $smush_image_url = preg_replace('/\.png$/', '.png.webp', $smush_image_url);
	        $smush_image_url = preg_replace('/\/uploads\//', '/smush-webp/', $smush_image_url);
	        $smush_image_file = str_replace(site_url() . "/", ABSPATH, $smush_image_url);

            // Does the webp file exist?
            // There is a separate cron job which checks for images that need converting to webp
            if (file_exists($smush_image_file)) {
                $image[0] = $smush_image_url;
            }
        }

    }
    return $image;
}
