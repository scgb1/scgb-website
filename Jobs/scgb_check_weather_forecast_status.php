<?php

use SCGB\Common;

require_once __DIR__ . '/../SCGB/Utils/scgb_autoload.php';

/**
 * Hourly check on Weather Forecast Data - check for any data more than 6 hours old
 *
 * @throws Exception
 */
function scgb_check_weather_forecast_status() : void
{
    global $wpdb;

    $status = 0;
    try{
        Common::scgb_initialise(__FUNCTION__);

        $sql = "SELECT min(dtmLastUpdate) from scgb_resort_forecast";
        $min = $wpdb->get_var($sql);
        if ($min === null) {
            throw new Exception('Failed to get min date');
        }
        $min = new DateTime($min);
        $now = new DateTime();
        $diff = $now->diff($min);
        if ($diff->h > 12) {
            Common::logger()->error('Some Weather Forecast data is more than 12 hours old',
                array('function' => __FUNCTION__, 'file' => basename(__FILE__), 'line' => __LINE__));
            $status = 1;
        }
    } catch (Exception $e){
        error_log(__FUNCTION__ . 'Failed to check Weather Forecast Status: ' . $e->getMessage());
        $status = 1;
    }
    try {
        Common::scgb_finalise($status);
    } catch (Exception $e) {
        error_log('Failed to finalise: ' . __FUNCTION__ . "/" . $e->getMessage());
    }
}
