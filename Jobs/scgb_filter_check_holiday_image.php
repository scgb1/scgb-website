<?php

const SCGB_HOLIDAY_NO_IMAGE_CHANGE_NEEDED = 0;
const SCGB_HOLIDAY_SOLD_OUT = 1;
const SCGB_HOLIDAY_DEPARTED = 2;
const SCGB_HOLIDAY_NEW = 3;

/**
 * See whether we need to add 'Sold Out', 'Departed' or 'New' to the holiday image.
 *
 * We know that the image is a holiday image because the size is 'holiday-thumbnail' - note that this could change!
 * We will know the availability by looking at the meta_key of 'holiday_price_note'
 *
 * If it is then we need to add the 'appropriate' icon to the image
 *
 * To avoid the chance of messing up production we catch any exceptions in this function and return the default image
 *
 * @param array|false $image
 * @param int $attachment_id
 * @param string|int[] $size
 * @return false|array
 * @noinspection PhpMissingParamTypeInspection
 * @noinspection PhpMissingReturnTypeInspection
 */
function scgb_filter_check_holiday_image($image, $attachment_id = null, $size = null)
{
    global $post;
    if ($post !== null && $post->ID !== null && $post->post_type == 'holiday' && $size == 'holiday-thumbnail') {
        $orig_image = $image;
        try {
            $holiday_status = scgb_get_holiday_status($post->ID);
            if ($holiday_status) {
                $holiday_thumbnail = get_post_meta($post->ID, "_thumbnail_id", true);
                if (intval($holiday_thumbnail) == $attachment_id) {
                    $image[0] = scgb_get_holiday_overlay_image_url($image[0], $holiday_status);
                }
            }
        } catch (Exception $e) {
            // Try to avoid breaking anything!
            error_log('scgb_filter_check_holiday_image - Exception: ' . $e->getMessage());
            $image = $orig_image;
        }
    }
    return $image;
}

/**
 * Determine whether the holiday is sold out, departed or new
 *
 * @param int $post_id
 * @return int
 * @throws Exception
 */
function scgb_get_holiday_status(int $post_id) : int
{
    $scgb_holiday_sold_out_text = get_option('scgb_holiday_sold_out_text');
    $scgb_holiday_departed_text = get_option('scgb_holiday_departed_text');
    $scgb_holiday_is_new = get_post_meta($post_id, 'holiday_is_new', true);

    if (!$scgb_holiday_sold_out_text) {
        error_log('scgb_is_holiday_available: scgb_holiday_sold_out_text not set - using default');
        $scgb_holiday_sold_out_text = 'No Holidays Available';
    }
    if (!$scgb_holiday_departed_text) {
        error_log('scgb_is_holiday_available: scgb_holiday_departed_text not set - using default');
        $scgb_holiday_departed_text = 'Departed';
    }
    $scgb_holiday_price_note = get_post_meta($post_id, 'holiday_price_note', true);

    if (!$scgb_holiday_price_note) {
        throw new Exception('scgb_is_holiday_available: holiday_price_note not set');
    }
    if ($scgb_holiday_price_note == $scgb_holiday_sold_out_text) {
        return SCGB_HOLIDAY_SOLD_OUT;
    } elseif ($scgb_holiday_price_note == $scgb_holiday_departed_text){
        return SCGB_HOLIDAY_DEPARTED;
    } elseif ($scgb_holiday_is_new == "yes") {
        return SCGB_HOLIDAY_NEW;
    }
    return SCGB_HOLIDAY_NO_IMAGE_CHANGE_NEEDED;
}

/**
 * Get URL of unavailable version of the holiday image - the featured image
 * If it doesn't exist then create it
 *
 * @param string $holiday_image_url
 * @param int $availability_type
 * @return string
 * @throws Exception
 */
function scgb_get_holiday_overlay_image_url(string $holiday_image_url, int $availability_type) : string
{
    // get the value of the WordPress option scgb_image_directory
    $scgb_image_directory = get_option('scgb_image_directory');
    if (!$scgb_image_directory) {
        throw new Exception('scgb_image_directory not set');
    }

    // Get source image file of the holiday primary image
    $holiday_image_path = str_replace(site_url() . "/", ABSPATH, $holiday_image_url);

    // Construct the sold out image name from the original image name so
    // image.jpg becomes image-sold-out.jpg
    $current_image = basename($holiday_image_path);
    if ($availability_type == SCGB_HOLIDAY_SOLD_OUT)
        $holiday_overlay_image_file = preg_replace('/(\.[^.]+)$/', '-sold-out$1', $current_image);
    elseif ($availability_type == SCGB_HOLIDAY_DEPARTED)
        $holiday_overlay_image_file = preg_replace('/(\.[^.]+)$/', '-departed$1', $current_image);
    elseif ($availability_type == SCGB_HOLIDAY_NEW)
        $holiday_overlay_image_file = preg_replace('/(\.[^.]+)$/', '-new$1', $current_image);
    else {
        throw new Exception('scgb_get_holiday_overlay_image_url: unknown availability type ' .
            $availability_type);
    }

    $sold_out_image_url = '/'. $scgb_image_directory . '/' . $holiday_overlay_image_file;

    // If the sold out image doesn't exist then create it
    // add the file system path to $sold_out_image
    $holiday_overlay_image_path = ABSPATH . $sold_out_image_url;
    if (!file_exists($holiday_overlay_image_path)) {
        scgb_create_holiday_overlay_image($holiday_image_path, $holiday_overlay_image_path, $availability_type);
    }

    // prepend with site url
    return site_url() . $sold_out_image_url;
}

/**
 * Create the overlay image - overlay the type image on top of the original image
 *
 * Overlay image needs to be 100% opacity
 * post_id of the overlay images is specified in one of wp options:
 * - scgb_sold_out_image_id
 * - scgb_departed_image_id
 * - scgb_new_image_id
 *
 * Take a naive approach to image sizing - we assume that the original image is 640×433
 *
 * @param string $holiday_image_path
 * @param string $amended_image_file
 * @param $availability_type
 * @return void
 * @throws Exception
 */
function scgb_create_holiday_overlay_image(string $holiday_image_path, string $amended_image_file, $availability_type) : void
{
    // get the value of the WordPress option scgb_sold_out_image_id
    if ($availability_type == SCGB_HOLIDAY_SOLD_OUT) {
        $holiday_overlay_icon_id = get_option('scgb_sold_out_image_id');
        if (!$holiday_overlay_icon_id) {
            throw new Exception('scgb_create_holiday_overlay_image: option scgb_sold_out_image_id not set');
        }
    } elseif ($availability_type == SCGB_HOLIDAY_DEPARTED){
        $holiday_overlay_icon_id = get_option('scgb_departed_image_id');
        if (!$holiday_overlay_icon_id) {
            throw new Exception('scgb_create_holiday_overlay_image: option scgb_departed_image_id not set');
        }
    } elseif ($availability_type == SCGB_HOLIDAY_NEW){
        $holiday_overlay_icon_id = get_option('scgb_new_image_id');
        if (!$holiday_overlay_icon_id) {
            throw new Exception('scgb_create_holiday_overlay_image: option scgb_new_image_id not set');
        }
    } else {
        throw new Exception('scgb_create_holiday_overlay_image: unknown availability type ' .
            $availability_type);
    }

    // We need to resize the holiday image to 640×433. We can't call wp_get_attachment_image_src because
    // We will end up in a recursive loop!!
    $new_width = 640;
    $new_height = 433;
    $image_type  = exif_imagetype($holiday_image_path);

    $orig = match ($image_type) {
        IMAGETYPE_JPEG => imagecreatefromjpeg($holiday_image_path),
        IMAGETYPE_PNG => imagecreatefrompng($holiday_image_path),
        IMAGETYPE_GIF => imagecreatefromgif($holiday_image_path),
        default => throw new Exception(
            'scgb_create_holiday_overlay_image: Unknown holiday image type: ' . $image_type),
    };

    $new = imagecreatetruecolor($new_width, $new_height);           // New blank image
    imagecopyresampled($new, $orig, 0, 0, 0, 0, $new_width,
        $new_height, $new_width, $new_height); // Crop and resize

    // get the url of the sold out image
    $sold_out_icon_path = get_attached_file($holiday_overlay_icon_id, true);
    $image_type = exif_imagetype($sold_out_icon_path);
    $sold_out_icon_image = match($image_type) {
        IMAGETYPE_JPEG => imagecreatefromjpeg($sold_out_icon_path),
        IMAGETYPE_PNG => imagecreatefrompng($sold_out_icon_path),
        IMAGETYPE_GIF => imagecreatefromgif($sold_out_icon_path),
        default => throw new Exception(
            'scgb_create_holiday_overlay_image: Unknown sold out image type: ' . $image_type),
    };

    // get the size of the sold out image
    $width= imagesx($sold_out_icon_image);
    $height = imagesy($sold_out_icon_image);

    // Overlay $sold_out_image on $image at top left corner with 100% opacity
    imagecopyresampled(
        $new, $sold_out_icon_image, 0, 0, 0, 0, $width, $height, $width, $height);

    // save the result
    imagejpeg($new, $amended_image_file);

    // Clean up
    imagedestroy($orig);
    imagedestroy($new);
    imagedestroy($sold_out_icon_image);
}
