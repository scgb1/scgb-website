<?php /** @noinspection ALL */

/*
 * Plugin Name:  SkiClub Website Support
 * Plugin URI:   https://gitlab.com/scgb1
 * Description:  Support functions for the SkiClub Website
 * Version:      1.2
 * Author:       Stu Bevan
 * Author URI:   https://gitlab.com/scgb1
*/

global $scgb_plugin_jobs;
$scgb_plugin_jobs = array(
//    'scgb_delete_expired_repping_slots' => array('type' => 'cron', 'frequency' => 'daily'),
    'scgb_filter_check_holiday_image' =>
        array('type' => 'filter', 'hook' => 'wp_get_attachment_image_src', 'priority' => 10, 'parameters' => 3),
    'scgb_filter_does_image_need_smushing' =>
        array('type' => 'filter', 'hook' => 'wp_get_attachment_image_src', 'priority' => 1000, 'parameters' => 1),
    'scgb_check_resort_data_status' => array('type' => 'cron', 'frequency' => 'hourly'),
    'scgb_check_weather_forecast_status' => array('type' => 'cron', 'frequency' => 'hourly'),
    'scgb_daily_check_resort_elevations' => array('type' => 'cron', 'frequency' => 'daily'),
    'scgb_daily_check_webp_images' => array('type' => 'cron', 'frequency' => 'daily'),
    'scgb_daily_keep_log_directory_clean' => array('type' => 'cron', 'frequency' => 'daily'),
    'scgb_daily_randomise_resort_order' => array('type' => 'cron', 'frequency' => 'daily'),
    'scgb_daily_update_discount_sort_order' => array('type' => 'cron', 'frequency' => 'daily'),
    'scgb_daily_update_search_statistics' => array('type' => 'cron', 'frequency' => 'daily'),
    'scgb_get_resort_weather' => array('type' => 'cron', 'frequency' => 'hourly'),
    'scgb_get_ski_service_data' => array('type' => 'cron', 'frequency' => 'hourly'),
    'scgb_process_sugati_tours' => array('type' => 'cron', 'frequency' => 'hourly'),
    'scgb_redbullet_refresh_sf' => array('type' => 'cron', 'frequency' => 'hourly'),
    'scgb_daily_check_azure_backups' => array('type' => 'cron', 'frequency' => 'daily'),
);

register_activation_hook(__FILE__, 'scgb_schedule_cron_jobs');
register_deactivation_hook( __FILE__, 'scgb_unschedule_cron_jobs' );

// Load and register the jobs
foreach ($scgb_plugin_jobs as $job => $job_details) {
    $file = __DIR__ . '/Jobs/' . $job . '.php';
    if (file_exists($file)) {
        require_once $file;
    }else {
        error_log('File ' . $file . ' does not exist');
    }

    if (!key_exists('type', $job_details)) {
        error_log('Job ' . $job . ' does not have a type');
        continue;
    }
    $job_type = $job_details['type'];
    if ($job_type == 'filter') {
        if (!key_exists('hook', $job_details)) {
            error_log('Job ' . $job . ' does not have a hook');
            continue;
        }
        $hook = $job_details['hook'];
        $priority = $job_details['priority'] ?? 10;
        add_filter($hook, $job, $priority, 4);
    } elseif ($job_type == 'cron') {
        if (!key_exists('frequency', $job_details)) {
            error_log('Job ' . $job . ' does not have a frequency');
            continue;
        }
        add_action($job . '_event', $job);
    } else {
        error_log('Job ' . $job . ' has an invalid type');
    }
}


/**
 * Schedule the cron jobs - standard stuff to make sure they only scheduled once
 */

function scgb_schedule_cron_jobs()
{
    global $scgb_plugin_jobs;

    // Schedule for 4am tomorrow
    $tomorrow = new DateTime('tomorrow 4am');

    // set schedule time to the nearest hour in the past
    $nearest_last_hour = time() - (time() % 3600);

    foreach ($scgb_plugin_jobs as $job => $job_details) {
        if ($job_details['type'] == 'cron') {
            if (!wp_next_scheduled($job . '_event')) {
                if ($job_details['frequency'] == 'daily') {
                    $schedule_time = $tomorrow->getTimestamp();
                } else {
                    // set schedule time to the nearest hour in the past
                    $schedule_time = $nearest_last_hour;
                }
                scgb_debug('scgb_schedule_cron_jobs - scheduling ' . $job . '_event');
                wp_schedule_event($schedule_time, $job_details['frequency'], $job . '_event');
            }
        }
    }
}

function scgb_unschedule_cron_jobs()
{
    global $scgb_plugin_jobs;
    foreach ($scgb_plugin_jobs as $job => $job_details) {
        if ($job_details['type'] == 'cron') {
            $timestamp = wp_next_scheduled($job . '_event');
            if ($timestamp) {
                scgb_debug('scgb_unschedule_cron_jobs - unscheduling ' . $job . '_event');
                wp_unschedule_event($timestamp, $job . '_event');
            }
        }
    }
}

/**
 * Create additional scgb cron schedules
 * - 4 times a day
 * @param $schedules
 * @return mixed
 */
function scgb_create_cron_schedules($schedules)
{
    $schedules['scgb_four_times_a_day'] = array(
        'interval' => 21600,
        'display' => __('Four times a day')
    );
    return $schedules;
}
add_filter('cron_schedules', 'scgb_create_cron_schedules');

/**
 * Debugging function - used when not possible to use the central SCGB logger
 * @param $message
 */
function scgb_debug($message) : void {
    if (WP_DEBUG === true) {
        // Add 'scgb plugin' to the message
        $message = 'scgb plugin: ' . $message;
        error_log($message);
    }
}

