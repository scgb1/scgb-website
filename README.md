# SCGB Website Plugin

A WordPress plugin containing support functions for the Ski Club of Great Britain (SCGB) 
website at https:/www.skiclub.co.uk.   The site was authored by 
[Redbullet,](https://redbullet.co.uk) and they implemented the bulk of the functionality
but there were some additional steps which required further work.

They are:
* Weather Forecast download
* Resort Conditions (runs open etc.) from SkiService
* Adding 'Sold Out and 'Departed' tags to Holiday images
* Ensuring the appropriate holiday price is displayed
* Randomising the order of resort, holidays and discounts
* Some image optimisation utilities 

The bulk of the documentation is in the repository [wiki](https://gitlab.com/scgb1/scgb-website/-/wikis/home).

This code is free for use for anyone who may have any remote use for it! 
This comes with no liability and no credits are required
